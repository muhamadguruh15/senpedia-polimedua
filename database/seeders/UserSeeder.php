<?php

namespace Database\Seeders;

use App\Models\User;
use GuzzleHttp\Promise\Create;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::create([
            'id' => 1,
            'profile_picture' => 'default.png',
            'first_name' => 'Admin',
            'middle_name' => null,
            'last_name' => 'Senpedia',
            'full_name'=> 'Admin Senpedia',
            'username' => 'admin-senpedia',
            'password' => Hash::make('123456'),
            'email' => '',
            'phone_number' => '',
            'gender' => '',
            'user_role' => 'Admin',
            'participant_code' => '',
            'send_email' => 0,
            'activation_email' => 0,
            'activation_code' => 0,
            'active' => 0,
        ]);
    }
}
