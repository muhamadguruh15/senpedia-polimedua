<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('paper_metadatas', function (Blueprint $table) {
            $table->increments("id");
            $table->unsignedInteger("paper_id");
            $table->string("first_name_author1", 32);
            $table->string("middle_name_author1", 32);
            $table->string("last_name_author1", 32);
            $table->string("email_author1", 64);
            $table->string("institution_author1", 64);
            $table->string("author_bio_statement1", 64);
            $table->string("first_name_author2", 32);
            $table->string("middle_name_author2", 32);
            $table->string("last_name_author2", 32);
            $table->string("email_author2", 64);
            $table->string("institution_author2", 64);
            $table->string("author_bio_statement2", 64);
            $table->string("first_name_author3", 32);
            $table->string("middle_name_author3", 32);
            $table->string("last_name_author3", 32);
            $table->string("email_author3", 64);
            $table->string("institution_author3", 64);
            $table->string("author_bio_statement3", 64);
            $table->string("first_name_author4", 32);
            $table->string("middle_name_author4", 32);
            $table->string("last_name_author4", 32);
            $table->string("email_author4", 64);
            $table->string("institution_author4", 64);
            $table->string("author_bio_statement4", 64);
            $table->string("first_name_author5", 32);
            $table->string("middle_name_author5", 32);
            $table->string("last_name_author5", 32);
            $table->string("email_author5", 64);
            $table->string("institution_author5", 64);
            $table->string("author_bio_statement5", 64);
            $table->string("first_name_author6", 32);
            $table->string("middle_name_author6", 32);
            $table->string("last_name_author6", 32);
            $table->string("email_author6", 64);
            $table->string("institution_author6", 64);
            $table->string("author_bio_statement6", 64);
            $table->string("first_name_author7", 32);
            $table->string("middle_name_author7", 32);
            $table->string("last_name_author7", 32);
            $table->string("email_author7", 64);
            $table->string("institution_author7", 64);
            $table->string("author_bio_statement7", 64);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('paper_metadatas');
    }
};
