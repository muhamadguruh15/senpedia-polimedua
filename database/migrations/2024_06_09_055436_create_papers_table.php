<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('papers', function (Blueprint $table) {
            $table->increments("id");
            $table->string("abstract_file", 256);
            $table->string("paper_code", 32);
            $table->string("category", 32);
            $table->string("title", 256);
            $table->text("abstract");
            $table->string("file_full_paper", 64)->nullable();
            $table->string("key_word", 64);
            $table->string("author_concerned", 64);
            $table->string("file_loa", 64)->nullable();
            $table->string("file_statement_of_originality", 64)->nullable();
            $table->string("confirmation_of_attendance", 18);
            $table->unsignedInteger("author_id");
            $table->unsignedInteger("reviewer_id");
            $table->string("status", 32);
            $table->tinyInteger("archive")->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('papers');
    }
};
