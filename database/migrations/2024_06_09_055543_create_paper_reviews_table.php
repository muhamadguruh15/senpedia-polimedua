<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('paper_reviews', function (Blueprint $table) {
            $table->increments("id");
            $table->unsignedInteger("paper_id");
            $table->string("admin_plagiarism_file", 128)->nullable();
            $table->string("author_plagiarism_file", 128)->nullable();
            $table->string("plagiarism_status", 32);
            $table->string("reviewer_revision_files", 128)->nullable();
            $table->string("author_revision_files", 128)->nullable();
            $table->string("status_revision", 32);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('paper_reviews');
    }
};
