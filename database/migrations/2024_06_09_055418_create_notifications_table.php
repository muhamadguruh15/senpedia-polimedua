<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->increments("id");
            $table->unsignedInteger("from_user_id");
            $table->unsignedInteger("to_user_id");
            $table->string("title", 64);
            $table->string("messages", 64);
            $table->string("url", 64);
            $table->unsignedInteger("paper_id");
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('notifications');
    }
};
