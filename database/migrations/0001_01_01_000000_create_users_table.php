<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments("id");
            $table->string('profile_picture', 64);
            $table->string('first_name', 16);
            $table->string('middle_name', 16)->nullable();
            $table->string('last_name', 16);
            $table->string('full_name', 255);
            $table->string('username', 32);
            $table->string('password', 64);
            $table->string('email', 64)->default('');
            $table->string('phone_number', 16)->default('');
            $table->string('gender', 16);
            $table->string('user_role', 16);
            $table->string('participant_code', 32)->default('');
            $table->tinyInteger('send_email');
            $table->tinyInteger('activation_email');
            $table->string('activation_code', 32)->default('');
            $table->tinyInteger('active');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('password_reset_tokens', function (Blueprint $table) {
            $table->string('email')->primary();
            $table->string('token');
            $table->timestamp('created_at')->nullable();
        });

        Schema::create('sessions', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->foreignId('user_id')->nullable()->index();
            $table->string('ip_address', 45)->nullable();
            $table->text('user_agent')->nullable();
            $table->longText('payload');
            $table->integer('last_activity')->index();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('password_reset_tokens');
        Schema::dropIfExists('sessions');
    }
};
