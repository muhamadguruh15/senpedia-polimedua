<?php

use App\Http\Controllers\AbstractController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\PenggunaController;
use App\Http\Controllers\ComunityServiceController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;


Route::get('/', function () {
    return view('home.index');
})->name('home');
Route::get('/beranda/admisi', function () {
    return view('home.admisi');
})->name('admisi');
Route::get('/beranda/pembayaran', function () {
    return view('home.payment');
})->name('payment');
Route::get('/beranda/kontak', function () {
    return view('home.contact');
})->name('contact');


Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
    Route::get('/template', [DashboardController::class, 'template'])->name('template');
    Route::post('/upload-file', [DashboardController::class, 'upload'])->name('file.upload');

    Route::get('/pengguna/{type}', [PenggunaController::class, 'index'])->name('pengguna.pengulas.index');
    Route::get('/pengguna/pengulas/form', [PenggunaController::class, 'form'])->name('pengguna.pengulas.form');
    Route::post('/pengguna', [PenggunaController::class, 'store'])->name('pengguna.store');

    Route::get('/abstrak', [AbstractController::class, 'index'])->name('abstract');
    Route::get('/abstrak/{id}', [AbstractController::class, 'showPaper'])->name('abstract.show');
    Route::get('/pembayaran', [AbstractController::class, 'indexPembayaran'])->name('pembayaran');
    Route::get('/paper/{type}', [ComunityServiceController::class, 'index'])->name('paper');
    Route::get('/paper', [ComunityServiceController::class, 'paper'])->name('paper.index');
    Route::get('/paper/detail/{id}', [ComunityServiceController::class, 'paperDetail'])->name('paper.show');

    Route::post('/ubah_periode_submisi', [DashboardController::class, 'ubahPeriodeSubmisi'])->name('update.submisi');
    Route::post('/ubah_periode_seleksi', [DashboardController::class, 'ubahPeriodeSeleksi'])->name('update.selection');
    Route::post('/ubah_periode_konferensi', [DashboardController::class, 'ubahPeriodeKonferensi'])->name('update.conference');

    Route::post('/abstrak', [AbstractController::class, 'storeAbstract'])->name('store.abstract');
    Route::post('/upload/pembayaran', [AbstractController::class, 'uploadBuktiPembayaran'])->name('upload.payment');
    Route::post('/update/pembayaran/{id}/{status}', [AbstractController::class, 'konfirmasiPembayaran'])->name('update.payment');
    Route::post('/update/reviewer/{id}', [ComunityServiceController::class, 'updateReviewer'])->name('update.reviewer');

    Route::get('/video', [ComunityServiceController::class, 'indexVideo'])->name('video');
    Route::get('/acara', [ComunityServiceController::class, 'indexAcara'])->name('acara');

    Route::get('/user/profile', [UserController::class, 'index'])->name('profile');
    Route::post('/user/profile/update', [UserController::class, 'profileUpdate'])->name('profile.update');
    Route::get('/user/pengaturan', [UserController::class, 'settings'])->name('settings');
    Route::post('/user/pengaturan/password', [UserController::class, 'updatePassword'])->name('settings.password');
});
