<?php

namespace App\Http\Controllers;

use App\Models\Notification;
use App\Models\Paper;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ComunityServiceController extends Controller
{
    protected $user;
    protected $notifications;
    public function __construct()
    {
        $this->user = Auth::user();
        if ($this->user->user_role != 'Admin') {
            $this->notifications = Notification::with(['paper'])->where('to_user_id', $this->user->id)->paginate(5);
        } else {
            $this->notifications = Notification::with(['paper', 'form'])->where('to_user_id', 0)->paginate(5);
        }
    }

    public function index($type)
    {
        $titles = [
            'PEKM' => 'Penguatan Ekonomi Kreatif Masyarakat',
            'PTPM' => 'Pemanfaatan Teknologi dalam Pemberdayaan Masyarakat',
            'PKMTKB' => 'Peningkatan Kepedulian Masyarakat Terhadap Kesenian dan Budaya',
            'PKMSB' => 'Pembangunan Kehidupan Masyarakat Sosial dan Berpendidikan',
            'PKMSP' => 'Pembangunan Kehidupan Masyarakat Sosial dan Berpendidikan'
        ];


        $user = Auth::user();
        $userRole = $user->user_role;
        $paper = [];
        switch ($userRole) {
            case 'Admin':
                $paper = Paper::where('category', $type)->get();
                break;
            default:
                $paper = Paper::where([
                    'category' => $type,
                    'reviewer_id' => $user->id
                ])->get();
                break;
        }
        $userInfo = $this->user;

        $notifications = $this->notifications;
        $subTitle = $titles[$type];
        $variableToView = ['subTitle', 'paper', 'notifications', 'userInfo'];

        return view('admin.paper', compact($variableToView));
    }

    public function paperDetail($id)
    {
        $userInfo = $this->user;
        $paper = Paper::with(['paper_metadatas', 'supplementary_file', 'video'])->find($id);
        $notifications = $this->notifications;
        $reviewers = User::where('user_role', 'Pengulas')->get();
        $variableToView = ['paper', 'reviewers', 'notifications', 'userInfo'];

        return view('admin.paper-detail', compact($variableToView));
    }

    public function updateReviewer(Request $request, $id)
    {
        $request->validate([
            'reviewer_id' => 'required'
        ]);

        $paper = Paper::find($id);
        if (!$paper) return redirect()->back()->with('error', 'Paper not found');

        $paper->reviewer_id = $request->reviewer_id;
        $paper->save();
        return redirect()->route('paper', $paper->category)->with('success', 'Reviewer berhasil dipilih');
    }

    public function paper()
    {
        $paper = [];
        $userInfo = $this->user;
        $notifications = $this->notifications;
        $variableToView = ['paper', 'notifications', 'userInfo'];
        return view('admin.paper-form', compact($variableToView));
    }

    public function indexVideo()
    {
        $video = [];
        $userInfo = $this->user;
        $notifications = $this->notifications;
        $variableToView = ['video', 'notifications', 'userInfo'];
        return view('admin.video', compact($variableToView));
    }

    public function indexAcara()
    {
        $userInfo = $this->user;
        $notifications = $this->notifications;
        $variableToView = ['notifications', 'userInfo'];
        return view('admin.acara', compact($variableToView));
    }
}
