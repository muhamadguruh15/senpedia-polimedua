<?php

namespace App\Http\Controllers;

use App\Models\Notification;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    protected $user;
    protected $notifications;
    public function __construct()
    {
        $this->user = Auth::user();
        if ($this->user->user_role != 'Admin') {
            $this->notifications = Notification::with(['paper'])->where('to_user_id', $this->user->id)->paginate(5);
        } else {
            $this->notifications = Notification::with(['paper', 'form'])->where('to_user_id', 0)->paginate(5);
        }
    }

    public function index()
    {
        $userInfo = $this->user;
        $notifications = $this->notifications;
        $variableToView = ['userInfo', 'notifications'];
        return view('admin.profile', compact($variableToView));
    }

    public function profileUpdate(Request $request)
    {
        $request->validate([
            'nama_depan' => 'required',
            'nama_belakang' => 'required',
            'email' => 'required|email',
            'gender' => 'required'
        ], [
            'nama_depan.required' => 'Nama depan harus diisi',
            'nama_belakang.required' => 'Nama belakang harus diisi',
            'email.required' => 'Email harus diisi',
            'gender.required' => 'Gender harus diisi'
        ]);

        $dataNama = [
            $request->input('nama_depan'),
            $request->input('nama_tengah'),
            $request->input('nama_belakang')
        ];
        $dataNamaLengkap = array_filter($dataNama);
        $namaLengkap = implode(" ", $dataNamaLengkap);

        $user = User::find($this->user->id);
        $user->first_name = $request->input('nama_depan');
        $user->middle_name = $request->input('nama_tengah');
        $user->last_name = $request->input('nama_belakang');
        $user->full_name = $namaLengkap;
        $user->email = $request->input('email');
        $user->gender = $request->input('gender');

        if (!$user->save()) {
            return redirect()->route('profile')->with('danger', 'Terjadi kesalahan');
        } else {
            return redirect()->route('profile')->with('success', 'Profil telah diubah');
        }
    }

    public function settings()
    {
        $userInfo = $this->user;
        $notifications = $this->notifications;
        $variableToView = ['notifications', 'userInfo'];
        return view('admin.pengaturan', compact($variableToView));
    }

    public function updatePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password_lama' => 'required',
            'password_baru' => 'required|min:6|max:18',
            'konfirmasi_password' => 'required|same:password_baru'
        ], [
            'password_lama.required' => 'Password lama harus diisi',
            'password_baru.required' => 'Password baru harus diisi',
            'password_baru.min' => 'Password minimal 6 karakter',
            'password_baru.max' => 'Password maksimal 18 karakter',
            'konfirmasi_password.required' => 'Konfirmasi password harus diisi',
            'konfirmasi_password.same' => 'Konfirmasi password tidak sama'
        ]);

        if ($validator->fails()) {
            return redirect()->route('settings')->withErrors($validator)->withInput();
        }

        $user = User::find($this->user->id);
        $passwordLama = $request->input('password_lama');
        $passwordBaru = $request->input('password_baru');

        if ($passwordLama == $passwordBaru) {
            return redirect()->route('settings')->with('danger', 'Password baru tidak boleh sama dengan password lama');
        } else {
            if (!Hash::check($passwordLama, $user->password)) {
                return redirect()->route('settings')->with('danger', 'Password lama salah');
            } else {
                $user->password = Hash::make($passwordBaru);

                if (!$user->save()) {
                    return redirect()->route('settings')->with('danger', 'Terjadi kesalahan');
                } else {
                    return redirect()->route('settings')->with('success', 'Password telah diubah');
                }
            }
        }
    }
}
