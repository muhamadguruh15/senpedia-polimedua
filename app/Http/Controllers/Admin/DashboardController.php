<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Notification;
use App\Models\Paper;
use App\Models\System;
use App\Models\TemplateFile;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class DashboardController extends Controller
{
    protected $user;
    protected $notifications;
    public function __construct()
    {
        $this->user = Auth::user();
        if ($this->user->user_role != 'Admin') {
            $this->notifications = Notification::with(['paper'])->where('to_user_id', $this->user->id)->paginate(5);
        } else {
            $this->notifications = Notification::with(['paper', 'form'])->where('to_user_id', 0)->paginate(5);
        }
    }

    public function index()
    {
        $userInfo = $this->user;
        $notifications = $this->notifications;

        $totalPaper = Paper::count();
        $totalPaperMe = Paper::where('reviewer_id', $userInfo->id)->count();

        $totalPengguna = User::count();
        $totalAbstrak = Paper::whereNotNull('file_full_paper')->count();
        $sistem = System::first();

        $variableToView = ['userInfo', 'notifications', 'totalPaper', 'totalPengguna', 'totalAbstrak', 'sistem', 'totalPaperMe'];
        return view('admin.dashboard', compact($variableToView));
    }

    public function ubahPeriodeSubmisi()
    {
        $system = System::first();

        $change = $system->submission == 1 ? 0 : 1;

        $system->submission = $change;
        $system->save();

        return redirect()->route('dashboard')->with('success', 'Periode submisi berhasil diubah');
    }

    public function ubahPeriodeSeleksi()
    {
        $system = System::first();
        $change = $system->selection == 1 ? 0 : 1;

        if ($change == 1) {
            $users = User::where('send_email', 0)->get();
            foreach ($users as $user) {
                $to = $user->email;
                $subject = 'Paper Anda Telah Disetujui';
                $message = '
                    Salam: ' . $user->full_name . ',
                    Terima kasih sudah mendaftarkan diri Bapak/Ibu/Saudara/i sebagai pemakalah pada Seminar Nasional Pengabdian Kepada Masyarakat (SENPEDIA) Politeknik Negeri Media Kreatif Tahun 2022 yang bertema “Membentuk Masyarakat Kreatif untuk Indonesia Bangga”. Abstraknya telah kami terima, kami harapkan Bapak/Ibu/Saudara/i untuk segera mengirimkan (submit) full paper sesuai dengan template berikut Template Makalah SENPEDIA2022 dan slide presentasi (ppt) melalui link https://bit.ly/Full-Paper-dan-PPT-Pemakalah-SENPEDIA. Kemudian melakukan pembayaran senilai Rp. 500.000 melalui
                    BCA 5465431358 a.n. Antinah Latif. Pengiriman full paper dan slide presentasi (ppt) serta pembayaran paling lambat tanggal 18 Oktober 2022.
                    <br>
                    Untuk konfirmasi pembayaran atau jika ada pertanyaan, silahkan hubungi narahubung kami, Yuyun Khairunnisa, M.T. melalui WA 085216511702 atau melalui email senpedia@polimedia.ac.id.
                    <br>
                    Bapak/Ibu/Saudara/i diharapkan bergabung pada WA Group berikut ini untuk memudahkan kami dalam memberikan informasi terkait hari-H 
                    Link Group Whatsapp
                    <br><br>
                    Thank you Best Regards,
                ';

                Mail::raw($message, function ($mail) use ($to, $subject) {
                    $mail->to($to == "" || $to == null ? "example@example.com" : $to)
                        ->from('senpedia@polimedia.ac.id', 'Admin Senpedia')
                        ->subject($subject);
                });

                $user->send_email = 1;
                $user->save();
            }
        }

        $system->selection = $change;
        $system->save();

        return redirect()->route('dashboard')->with('success', 'Periode seleksi berhasil diubah');
    }

    public function ubahPeriodeKonferensi()
    {
        $system = System::first();
        $change = $system->conference == 1 ? 0 : 1;
        $system->conference = $change;
        $system->save();

        return redirect()->route('dashboard')->with('success', 'Periode konferensi berhasil diubah');
    }

    public function template()
    {
        $userInfo = $this->user;
        $template = TemplateFile::get();
        $notifications = $this->notifications;
        $variableToView = ['template', 'notifications', 'userInfo'];
        return view('admin.template', compact($variableToView));
    }

    public function upload(Request $request)
    {
        $request->validate([
            'file_template' => 'required|mimes:doc,docx,application/msword|max:2048',
        ]);

        if ($request->file()) {
            $fileName = time() . '_' . $request->file_template->getClientOriginalName();
            $request->file('file_template')->storeAs('uploads/template', $fileName, 'public');

            $file = TemplateFile::find($request->id_template);
            $file->file = $fileName;
            $file->save();

            return back()
                ->with('success', 'File telah diupload.')
                ->with('file', $fileName);
        }
    }
}
