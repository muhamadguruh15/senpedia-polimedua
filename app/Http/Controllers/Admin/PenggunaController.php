<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Notification;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class PenggunaController extends Controller
{
    protected $user;
    protected $notifications;
    public function __construct()
    {
        $this->user = Auth::user();
        if ($this->user->user_role != 'Admin') {
            $this->notifications = Notification::with(['paper'])->where('to_user_id', $this->user->id)->paginate(5);
        } else {
            $this->notifications = Notification::with(['paper', 'form'])->where('to_user_id', 0)->paginate(5);
        }
    }

    public function index($role)
    {
        $userInfo = $this->user;
        $subTitle = ucfirst($role);
        $pengguna = User::where('user_role', $role)->get();
        $notifications = $this->notifications;
        $variableToView = ['subTitle', 'pengguna', 'notifications', 'userInfo'];
        return view('admin.pengguna', compact($variableToView));
    }

    public function form()
    {
        $userInfo = $this->user;
        $notifications = $this->notifications;
        $variableToView = ['notifications', 'userInfo'];
        return view('admin.pengguna-form', compact($variableToView));
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama_depan' => 'required',
            'nama_belakang' => 'required',
            'email' => 'required',
            'gender' => 'required',
            'username' => 'required',
            'password' => 'required'
        ]);

        $fullname = $request->nama_depan . ' ' . $request->nama_belakang;
        User::create([
            'profile_picture' => 'default.png',
            'first_name' => $request->nama_depan,
            'middle_name' => $request->nama_tengah,
            'last_name' => $request->nama_belakang,
            'full_name' => $fullname,
            'username' => $request->username,
            'password' => Hash::make($request->password),
            'email' => $request->email,
            'phone_number' => '',
            'gender' => $request->gender,
            'user_role' => 'Pengulas',
            'participant_code' => '',
            'send_email' => 0,
            'activation_email' => 0,
            'activation_code' => 0,
            'active' => 0,
        ]);

        return redirect()->route('pengguna.pengulas.index', 'Pengulas')->with(['success' => 'Data Berhasil Disimpan!']);
    }
}
