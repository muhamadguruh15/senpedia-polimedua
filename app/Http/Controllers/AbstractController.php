<?php

namespace App\Http\Controllers;

use App\Models\Notification;
use App\Models\Paper;
use App\Models\PaperMetadata;
use App\Models\PaperSupplementaryFile;
use App\Models\PaperVideo;
use App\Models\User;
use App\Models\UserPayment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class AbstractController extends Controller
{
    protected $user;
    protected $notifications;
    public function __construct()
    {
        $this->user = Auth::user();
        if ($this->user->user_role != 'Admin') {
            $this->notifications = Notification::with(['paper'])->where('to_user_id', $this->user->id)->paginate(5);
        } else {
            $this->notifications = Notification::with(['paper', 'form'])->where('to_user_id', 0)->paginate(5);
        }
    }

    public function index()
    {
        $role = Auth::user()->user_role;
        $abstracts = Paper::where('file_full_paper', null)->get();
        $notifications = $this->notifications;
        $view = "";
        switch ($role) {
            case 'Pengarang':
                $view  = 'admin.abstrak-form';
                break;
            default:
                $view = 'admin.abstrak';
                break;
        }
        $userInfo = $this->user;
        $variableToView = ['abstracts', 'notifications', 'userInfo'];

        return view($view, compact($variableToView));
    }

    public function storeAbstract(Request $request)
    {
        $request->validate([
            'file_abstrak' => 'required|mimes:doc,docx,application/msword|max:2048',
            'kategori' => 'required',
            'judul' => 'required',
            'abstrak' => 'required',
            'kata_kunci' => 'required',
            'pengarang_bersangkutan' => 'required',
            'nama_depan_pengarang1' => 'required',
            'nama_belakang_pengarang1' => 'required',
            'institusi_pengarang1' => 'required',
            'pernyataan_bio_pengarang1' => 'required',
        ]);

        DB::transaction(function () use ($request) {
            $user = Auth::user();
            $abstract = new Paper();
            $abstract->category = $request->kategori;
            $abstract->title = $request->judul;
            $abstract->abstract = $request->abstrak;
            $abstract->key_word = $request->kata_kunci;
            $abstract->paper_code = '';
            $abstract->author_concerned = $request->pengarang_bersangkutan;
            $abstract->confirmation_of_attendance = '';
            $abstract->author_id = $user->id;
            $abstract->status = 'Sedang diproses';
            $abstract->reviewer_id = 0;

            if (!$request->file()) return redirect()->route('abstract')->with('danger', 'Abstract gagal dibuat');

            $fileName = time() . '_' . $request->file_abstrak->getClientOriginalName();
            $request->file('file_abstrak')->storeAs('uploads/file-abstract', $fileName, 'public');

            $abstract->abstract_file = $fileName;
            $abstract->save();

            $metadata = new PaperMetadata();
            $metadata->paper_id = $abstract->id;
            for ($i = 1; $i <= 7; $i++) {
                $metadata['first_name_author' . $i] = $request['nama_depan_pengarang' . $i] ?? '';
                $metadata['middle_name_author' . $i] = $request['nama_tengah_pengarang' . $i] ?? '';
                $metadata['last_name_author' . $i] = $request['nama_belakang_pengarang' . $i] ?? '';
                $metadata['email_author' . $i] = $request['email_pengarang' . $i] ?? '';
                $metadata['institution_author' . $i] = $request['institusi_pengarang' . $i] ?? '';
                $metadata['author_bio_statement' . $i] = $request['pernyataan_bio_pengarang' . $i] ?? '';
            }

            $metadata->save();

            $videoModel = new PaperVideo();
            $videoModel->paper_id = $abstract->id;
            $videoModel->link_video = '';
            $videoModel->save();

            $suplementary = new PaperSupplementaryFile();
            $suplementary->paper_id = $abstract->id;
            $suplementary->save();

            Notification::create([
                'from_user_id' => $user->id,
                'to_user_id' => 0,
                'title' => 'Abstrak Disubmit',
                'messages' => 'telah mensubmit abstrak',
                'url' => '/abstrak/' . $abstract->id,
                'paper_id' => $abstract->id
            ]);
        });

        return redirect()->route('abstract')->with('success', 'Abstract berhasil dibuat');
    }

    public function showPaper($id)
    {
        $userInfo = $this->user;
        $abstract = Paper::with('paper_metadatas')->find($id);
        $notifications = $this->notifications;
        $variableToView = ['abstract', 'notifications', 'userInfo'];
        return view('admin.abstrak-detail', compact($variableToView));
    }

    public function indexPembayaran()
    {
        $view = "";
        $role = $this->user->user_role;
        $pembayaran = [];
        switch ($role) {
            case "Admin":
                $pembayaran = UserPayment::with("user")->get();
                $view = "admin.pembayaran";
                break;
            default:
                $view = "admin.pembayaran-form";
                $pembayaran = UserPayment::with('user')->where('user_id', $this->user->id)->first();
                break;
        }


        $userInfo = $this->user;
        $notifications = $this->notifications;
        $abstrak = Paper::where('author_id', $this->user->id)->first();
        $variableToView = ['pembayaran', 'userInfo', 'notifications', 'abstrak'];
        return view($view, compact($variableToView));
    }

    public function uploadBuktiPembayaran(Request $request)
    {
        $request->validate([
            'bukti_pembayaran' => [
                'required',
                'file',
                'max:5120',
                'mimes:jpg,jpeg,png',
            ]
        ], [
            'bukti_pembayaran.required' => 'Pilih gambar terlebih dahulu',
            'bukti_pembayaran.max' => 'File maksimal berukuran 5MB',
            'bukti_pembayaran.mimes' => 'Ekstensi file tidak didukung',
        ]);

        $fileBuktiPembayaran = $request->file('bukti_pembayaran');
        $namaFile = $request->input('bukti_pembayaran');

        if ($fileBuktiPembayaran) {
            if ($request->filled('bukti_pembayaran_lama')) {
                Storage::delete('images/bukti-pembayaran/' . $request->input('bukti_pembayaran_lama'));
            }
            $namaFile = $fileBuktiPembayaran->store('images/bukti-pembayaran', 'public');
        }

        $query = UserPayment::updateOrCreate(
            ['id' => $request->input('id')],
            [
                'proof_of_payment' => str_replace('images/bukti-pembayaran/', '', $namaFile),
                'status' => 'Menunggu konfirmasi'
            ]
        );

        Notification::create([
            'from_user_id' => Auth::id(),
            'to_user_id' => 0,
            'title' => 'Bukti Pembayaran Diterima',
            'messages' => 'telah mengirim bukti pembayaran',
            'url' => '/pembayaran',
            'paper_id' => 0
        ]);

        if (!$query) {
            return redirect()->route('pembayaran')->with('danger', 'Terjadi kesalahan');
        } else {
            return redirect()->route('pembayaran')->with('success', 'Bukti pembayaran telah dikirim');
        }
    }

    public function konfirmasiPembayaran(Request $request, $id, $status)
    {
        $idUser = $request->input('user_id');
        $user = User::find($idUser);

        if ($user && $user->used_role == 'Peserta') {
            $lastParticipantPayment = UserPayment::where('role', 'Peserta')->count();
            $currentParticipantPayment = $lastParticipantPayment + 1;
            $participantCode = 'SENPEDIA/P/' . str_pad($currentParticipantPayment, 3, '0', STR_PAD_LEFT) . '/' . date("Y");

            $to = $user->email;
            $subject = 'Pembayan Anda Telah Dikonfirmasi';
            $message = 'Dear Participant(s): ' . $user->nama_lengkap . "\r\n\r\n" .
                "On behalf of the 1st Jakarta International Conference on Multidisciplinary Studies (JICoMS) 2022 committee, we are pleased to inform you that your registration for participant JICoMS has been success. Furthermore, we will send back link zoom to attend the conference virtually. If you want to attend this conference in person, please show this email at the registration desk. Visit our website for further information.\r\n\r\n" .
                "Thank you Best Regards,\r\n\r\n" .
                "Dr. Handika Dany Rahmayanti, M.Si\r\n" .
                "JICoMS 2022 Conference Chair.";

            Mail::raw($message, function ($mail) use ($to, $subject) {
                $mail->to($to)
                    ->from('senpedia@polimedia.ac.id', 'Admin Senpedia')
                    ->subject($subject);
            });

            $user->update([
                'kode_peserta' => $participantCode,
                'email_dikirim' => 1
            ]);
        }

        $payment = UserPayment::find($request->input('id'));
        $payment->update([
            'status' => 'Dikonfirmasi'
        ]);

        Notification::create([
            'from_user_id' => 0,
            'to_user_id' => $idUser,
            'title' => 'Pembayaran Dikonfirmasi',
            'messages' => 'Bukti pembayaran telah dikonfirmasi',
            'url' => '/pembayaran',
            'paper_id' => 0
        ]);

        if (!$payment) {
            return redirect()->route('pembayaran')->with('danger', 'Terjadi kesalahan');
        } else {
            return redirect()->route('pembayaran')->with('success', 'Pembayaran dikonfirmasi');
        }
    }
}
