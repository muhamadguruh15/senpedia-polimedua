<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaperMetadata extends Model
{
    protected $table = 'paper_metadatas';
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'paper_id',
        'first_name_author1',
        'middle_name_author1',
        'last_name_author1',
        'email_author1',
        'institution_author1',
        'author_bio_statement1',
        'first_name_author2',
        'middle_name_author2',
        'last_name_author2',
        'email_author2',
        'institution_author2',
        'author_bio_statement2',
        'first_name_author3',
        'middle_name_author3',
        'last_name_author3',
        'email_author3',
        'institution_author3',
        'author_bio_statement3',
        'first_name_author4',
        'middle_name_author4',
        'last_name_author4',
        'email_author4',
        'institution_author4',
        'author_bio_statement4',
        'first_name_author5',
        'middle_name_author5',
        'last_name_author5',
        'email_author5',
        'institution_author5',
        'author_bio_statement5',
        'first_name_author6',
        'middle_name_author6',
        'last_name_author6',
        'email_author6',
        'institution_author6',
        'author_bio_statement6',
        'first_name_author7',
        'middle_name_author7',
        'last_name_author7',
        'email_author7',
        'institution_author7',
        'author_bio_statement7',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array<int, string>
     */
    protected $appends = [];

    /**
     * The attributes that should be cast.
     *
     * @return array<string, string>
     */
    protected function casts(): array
    {
        return [];
    }

    /**
     * Get the paper associated with the metadata.
     */
    public function paper()
    {
        return $this->belongsTo(Paper::class, 'paper_id');
    }
}
