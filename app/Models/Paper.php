<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Paper extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'abstract_file',
        'paper_code',
        'category',
        'title',
        'abstract',
        'file_full_paper',
        'key_word',
        'author_concerned',
        'file_loa',
        'file_statement_of_originality',
        'confirmation_of_attendance',
        'author_id',
        'reviewer_id',
        'status',
        'archive',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array<int, string>
     */
    protected $appends = [];

    /**
     * The attributes that should be cast.
     *
     * @return array<string, string>
     */
    protected function casts(): array
    {
        return [
            'archive' => 'boolean',
        ];
    }

    public function author()
    {
        return $this->hasOne(User::class, 'id', 'author_id');
    }

    public function reviewer()
    {
        return $this->hasOne(User::class, 'id', 'reviewer_id');
    }

    public function authors()
    {
        return $this->hasMany(PaperAuthor::class);
    }

    public function reivew()
    {
        return $this->hasOne(PaperReview::class);
    }

    public function supplementary_file()
    {
        return $this->hasOne(PaperSupplementaryFile::class, 'paper_id', 'id');
    }

    public function video()
    {
        return $this->hasOne(PaperVideo::class, 'paper_id', 'id');
    }

    public function paper_metadatas()
    {
        return $this->hasOne(PaperMetadata::class, 'paper_id', 'id');
    }
}
