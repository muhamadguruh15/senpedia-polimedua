@extends('layouts.home')
@section('content')
    <!--MAIN BANNER AREA -->
    <div class="banner-area banner-2" id="kontak">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 m-auto text-center col-sm-12 col-md-12">
                            <div class="banner-content content-padding">
                                <h1 class="banner-title">Kontak</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--MAIN HEADER AREA END -->

    <!--  Contact START  -->
    <section class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-sm-12 col-md-12">
                    <div class="mb-5">
                        <h2 class="mb-2">Hubungi Kami</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-7 col-sm-12">
                    <form class="contact__form" method="post" action="mail.php">
                        <!-- form message -->
                        <div class="row">
                            <div class="col-12">
                                <div class="alert alert-success contact__msg" style="display: none" role="alert">
                                    Pesan anda berhasil dikirim.
                                </div>
                            </div>
                        </div>
                        <!-- end message -->
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <input name="name" type="text" class="form-control" placeholder="Nama" required>
                            </div>
                            <div class="col-md-6 form-group">
                                <input name="email" type="email" class="form-control" placeholder="Email" required>
                            </div>
                            <div class="col-md-12 form-group">
                                <input name="subjek" type="text" class="form-control" placeholder="Subjek" required>
                            </div>
                            <div class="col-12 form-group">
                                <textarea name="pesan" class="form-control" rows="6" placeholder="Pesan" required></textarea>
                            </div>
                            <div class="col-12 mt-4">
                                <input name="submit" type="submit" class=" btn btn-hero btn-circled" value="Kirim Pesan">
                            </div>
                        </div>
                    </form>
                </div>

                <div class="col-lg-5 pl-4 mt-4 mt-lg-0">
                    <h4>Alamat: </h4>
                    <p class="mb-3">Jl. Srengseng Sawah Raya No.17, RT.8/RW.3, Srengseng Sawah, Kec. Jagakarsa, Kota
                        Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12630</p>
                    <h4>No. Telp: </h4>
                    <p class="mb-3">+62-21-7864753/55</p>
                    <h4>E-Mail: </h4>
                    <p class="mb-3">senpedia@polimedia.ac.id</p>
                    <h4>Website: </h4>
                    <p>senpedia.polimedia.ac.id</p>
                </div>
            </div>
        </div>
    </section>
    <!--  CONTACT END  -->
@endsection
