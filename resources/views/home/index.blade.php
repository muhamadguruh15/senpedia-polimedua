@extends('layouts.home')
@section('content')
    <!--MAIN BANNER AREA -->
    <div class="banner-area banner-2" id="beranda">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 m-auto text-center col-sm-12 col-md-12">
                            <div class="banner-content content-padding">
                                <h1 class="banner-title">Seminar Nasional</h1>
                                <h3 class="banner-title"><i>Pengabdian Kepada Masyarakat</i></h3>
                                <h3 class="banner-title"><i>Politeknik Negeri Media Kreatif</i></h3>
                                <h4 class="banner-title"><i>SENPEDIA 2022</i></h4>
                                <h5 class="subtitle">-Hybrid-</h5>
                                <p>Politeknik Negeri Media Kreatif | Depok, 2 November 2022</p>

                                <a href="https://docs.google.com/document/d/1xNK4jVwh690MehPRMnnfkyk6pI8F2IgZ/edit?usp=sharing&ouid=114487192761961237400&rtpof=true&sd=true"
                                    class="btn btn-white btn-circled">Unduh Format Semnas</a>
                                <a href="https://docs.google.com/document/d/1PfE3tF0QO_aMHPNSJdtr6JxgHFvAsbEA/edit?usp=sharing&ouid=114487192761961237400&rtpof=true&sd=true"
                                    class="btn btn-white btn-circled">Unduh Abstrak</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--MAIN HEADER AREA END -->

    <!--  ABOUT AREA START  -->
    <section id="intro" class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-md-12">
                    <div class="section-heading">
                        <h4 class="section-title">Call For Paper</h4>
                        <p class="lead">Membentuk Masyarakat Kreatif untuk Indonesia Bangga</p>
                        <a href="contact.html" class="btn btn-warning btn-circled">Unduh Format CFP</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--  ABOUT AREA END  -->

    <!--  SERVICE AREA START  -->
    <section id="service">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12 m-auto">
                    <div class="section-heading">
                        <h4 class="section-title"></h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-sm-6 col-md-6">
                    <div class="service-box">
                        <div class="service-img-icon">
                            <img src="{{ asset('assets/promodise-main/images/icon/007-digital-marketing-3.png') }}"
                                alt="service-icon" class="img-fluid">
                        </div>
                        <div class="service-inner">
                            <h4>Biaya</h4>
                            <p>Rp 500.000/Peserta Pemakalah (Luring dan Daring) *Gratis biaya pendaftaran bagi 10
                                pendaftar pertama bagi presenter luring</p>
                            <p>Rp 300.000/Peserta Non-Pemakalah (Luring dan Daring)</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 col-md-6">
                    <div class="service-box ">
                        <div class="service-img-icon">
                            <img src="{{ asset('assets/promodise-main/images/icon/008-digital-marketing-2.png') }}"
                                alt="service-icon" class="img-fluid">
                        </div>
                        <div class="service-inner">
                            <h4>Publikasi Prosiding</h4>
                            <p>Setiap naskah yang diterima akan diterbitkan melalui prosiding online berISBN tanpa
                                pemungutan biaya tambahan (GRATIS)</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 col-md-6">
                    <div class="service-box">
                        <div class="service-img-icon">
                            <img src="{{ asset('asstes/promodise-main/images/icon/003-task.png') }}" alt="service-icon"
                                class="img-fluid">
                        </div>
                        <div class="service-inner">
                            <h4>Makalah Terbaik</h4>
                            <p>Makalah terbaik akan diterbitkan pada Jurnal Pengabdian Kepada Masyarakat "Jurnal
                                PEKAMAS" dan partner jurnal Polimedia (Terakreditasi SINTA) setelah melalui proses
                                review</p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-sm-6 col-md-6">
                    <div class="service-box">
                        <div class="service-img-icon">
                            <img src="{{ asset('assets/promodise-main/images/icon/010-digital-marketing.png') }}"
                                alt="service-icon" class="img-fluid">
                        </div>
                        <div class="service-inner">
                            <h4>Sertifikat</h4>
                            <p>Seluruh pemakalah dan peserta mendapatkan sertifikat</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 col-md-6">
                    <div class="service-box">
                        <div class="service-img-icon">
                            <img src="{{ asset('assets/promodise-main/images/icon/006-analytics.png') }}" alt="service-icon"
                                class="img-fluid">
                        </div>
                        <div class="service-inner">
                            <h4>Hadiah Menarik</h4>
                            <p>Makalah dan Pemakalah terbaik akan diberikan hadiah</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--  SERVICE AREA END  -->

    <!--  SERVICE AREA START  -->
    <section id="service">
        <div class="container mt-4">
            <div class="row">
                <div class="col-lg-12 col-sm-12 m-auto">
                    <div class="section-heading">
                        <h4 class="section-title">Sub Tema</h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-sm-6 col-md-6">
                    <div class="service-box">
                        <div class="service-img-icon">
                            <img src="{{ asset('assets/promodise-main/images/icon/003-task.png') }}" alt="service-icon"
                                class="img-fluid">
                        </div>
                        <div class="service-inner">
                            <h4>Ekonomi Kreatif</h4>
                            <p>Penguatan Ekonomi Kreatif Masyarakat</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 col-md-6">
                    <div class="service-box">
                        <div class="service-img-icon">
                            <img src="{{ asset('assets/promodise-main/images/icon/007-digital-marketing-3.png') }}"
                                alt="service-icon" class="img-fluid">
                        </div>
                        <div class="service-inner">
                            <h4>Teknologi</h4>
                            <p>Pemanfaatan Teknologi dalam Pemberdayaan Masyarakat</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 col-md-6">
                    <div class="service-box ">
                        <div class="service-img-icon">
                            <img src="{{ asset('assets/promodise-main/images/icon/008-digital-marketing-2.png') }}"
                                alt="service-icon" class="img-fluid">
                        </div>
                        <div class="service-inner">
                            <h4>Seni dan Budaya</h4>
                            <p>Peningkatan Kepedulian Masyarakat Terhadap Kesenian Dan Budaya</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 col-md-6">
                    <div class="service-box ">
                        <div class="service-img-icon">
                            <img src="{{ asset('assets/promodise-main/images/icon/008-digital-marketing-2.png') }}"
                                alt="service-icon" class="img-fluid">
                        </div>
                        <div class="service-inner">
                            <h4>Sosial dan Pendidikan</h4>
                            <p>Pembangunan Kehidupan Masyarakat Sosial dan Berpendidikan</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--  SERVICE AREA END  -->


    <!--  SERVICE AREA START  -->
    <section id="service">
        <div class="container mt-4">
            <div class="row">
                <div class="col-lg-12 col-sm-12 m-auto">
                    <div class="section-heading">
                        <h4 class="section-title">Tanggal Penting</h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-md-12">
                    <div class="timeline">
                        <div class="timeline-container left">
                            <div class="timeline-content text-right">
                                <h2>29 Juli – 15 Oktober 2022</h2>
                                <p>Pendaftaran Pemakalah/Non-Pemakalah dan pengumpulan abstrak</p>
                            </div>
                        </div>
                        <div class="timeline-container right">
                            <div class="timeline-content text-left">
                                <h2>16 Oktober 2022</h2>
                                <p>Pengumuman penerimaan abstrak</p>
                            </div>
                        </div>
                        <div class="timeline-container left">
                            <div class="timeline-content text-right">
                                <h2>18 Oktober 2022</h2>
                                <p>Batas akhir pembayaran seminar</p>
                            </div>
                        </div>
                        <div class="timeline-container right">
                            <div class="timeline-content text-left">
                                <h2>26 Oktober 2022</h2>
                                <p>Batas pengumpulan naskah lengkap</p>
                            </div>
                        </div>
                        <div class="timeline-container left">
                            <div class="timeline-content text-right">
                                <h2>28 Oktober 2022</h2>
                                <p>Batas pengumpulan slide presentasi bagi pemakalah dan video presentasi bagi peserta
                                    daring</p>
                            </div>
                        </div>
                        <div class="timeline-container right">
                            <div class="timeline-content text-left">
                                <h2>2 November 2022</h2>
                                <p>Pelaksanaan Seminar Nasional Pengabdian Kepada Masyarakat SENPEDIA 2022</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--  COUNTER AREA END  -->

    <!--  ABOUT AREA START  -->
    <section id="panitia" class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12 m-auto">
                    <div class="section-heading">
                        <h4 class="section-title">Panitia</h4>
                        <div class="line"></div>
                    </div>
                </div>
            </div>
            <div class="row mb-5">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="row">
                        <div class="col-lg-4 col-sm-4 col-md-4">
                        </div>
                        <div class="col-lg-4 col-sm-4 col-md-4">
                            <div class="intro-box">
                                <h4>Pengarah</h4>
                                <p>Direktur (Dr. Tipri Rose Kartika., M.M.)</p>

                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-4 col-md-4">
                        </div>
                        <div class="col-lg-4 col-sm-4 col-md-4">
                            <div class="intro-box">
                                <h4>Penanggungjawab</h4>
                                <p>Dr. Handika Dany Rahmayanti, M.Si.</p>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-4 col-md-4">
                            <div class="intro-box">
                                <h4>Ketua</h4>
                                <p>Widi Sriyanto, S.Pd. M.Pd</p>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-4 col-md-4">
                            <div class="intro-box">
                                <h4>Wakil Ketua</h4>
                                <p>Nurul Akmalia, M.Med.Kom.</p>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-4 col-md-4">
                            <div class="intro-box">
                                <h4>Seksi Acara</h4>
                                <p>Pathiya, S.I.Kom.,M.I.Kom</p>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-4 col-md-4">
                            <div class="intro-box">
                                <h4>Seksi Desain & Promosi</h4>
                                <p>Yessy Yerta Situngkir,S.T., M.M.</p>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-4 col-md-4">
                            <div class="intro-box">
                                <h4>Seksi IT</h4>
                                <p>Cholid Mawardi, S.Kom., M.T.</p>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-4 col-md-4">
                            <div class="intro-box">
                                <h4>Seksi Kesekretariatan</h4>
                                <p>Yuyun Khairunisa, S.Si.,M.Kom.</p>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-4 col-md-4">
                            <div class="intro-box">
                                <h4>Seksi Logistik</h4>
                                <p>Habibi Santoso, S.T. M.T. </p>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-4 col-md-4">
                            <div class="intro-box">
                                <h4>Seksi Konsumsi</h4>
                                <p>Septia Ardani, S.Si., M.Si.</p>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-2 col-md-2">
                        </div>
                        <div class="col-lg-4 col-sm-4 col-md-4">
                            <div class="intro-box">
                                <h4>Seksi Publikasi Artikel</h4>
                                <p>Henra Nanang Sukma, S.T., M.T.</p>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-4 col-md-4">
                            <div class="intro-box">
                                <h4>Bendahara</h4>
                                <p>Antinah Latif, S.K.M, M.K.M.</p>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-2 col-md-2">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--  ABOUT AREA END  -->

    <!--  BLOG AREA START  -->
    <section id="blog" class="section-padding bg-main">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12 m-auto">
                    <div class="section-heading">
                        <h4 class="section-title">Speaker</h4>
                        <div class="line"></div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-3 col-sm-6 col-md-3">
                    <div class="blog-block ">
                        <img src="{{ asset('assets/promodise-main/images/blog/narsum elisabet.png') }}" alt=""
                            class="w-100">
                        <div class="blog-text">
                            <h6 class="author-name"><span>Ir. Elisabeth Ratu Rante Allo, M.M., M.Si.</span></h6>
                            <p class="my-2 d-inline-block">(Kepala Dinas Koperasi dan UMKM Jakarta)</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 col-md-3">
                    <div class="blog-block ">
                        <img src="{{ asset('assets/promodise-main/images/blog/narsum faisal.png') }}" alt=""
                            class="w-100">
                        <div class="blog-text">
                            <h6 class="author-name"><span>Prof. Teuku Faisal Fathani, Ph.D., IPU., ASEAN Eng</span>
                            </h6>
                            <p class="my-2 d-inline-block">(Direktur Riset, Teknologi dan Pengabdian Kepada Masyarakat)
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 col-md-3">
                    <div class="blog-block ">
                        <img src="{{ asset('assets/promodise-main/images/blog/narsum nurul.png') }}" alt=""
                            class="w-100">
                        <div class="blog-text">
                            <h6 class="author-name"><span>Prof. Dr. Nurul Taufiqu Rochman M.Eng</span></h6>
                            <p class="my-2 d-inline-block">(Komisaris Utama Nanotech Indonesia Global)</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6 col-md-3">
                    <div class="blog-block ">
                        <img src="{{ asset('assets/promodise-main/images/blog/narsum indra sutisna.png') }}"
                            alt="" class="w-100">
                        <div class="blog-text">
                            <h6 class="author-name"><span>Imron Yunus</span></h6>
                            <p class="my-2 d-inline-block">Kepala UPK Perkampungan Budaya Betawi)</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--  BLOG AREA END  -->


    <!--  SERVICE AREA START  -->
    <section id="about" class="bg-light">
        <div class="about-bg-img d-none d-lg-block d-md-block"></div>

        <div class="container">
            <div class="row">

                <div class="col-lg-7 col-sm-12 col-md-8">
                    <div class="about-content">
                        <h5 class="subtitle">Venue</h5>
                        <h3>Savero Hotel Depok</h3>
                        <p>Jl. Margonda Raya No.230A, Kemiri Muka, Kecamatan Beji, Kota Depok, Jawa Barat 16423</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--  SERVICE AREA END  -->
@endsection
