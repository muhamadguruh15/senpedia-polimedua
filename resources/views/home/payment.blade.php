@extends('layouts.home')
@section('content')
    <!--MAIN BANNER AREA -->
    <div class="banner-area banner-2" id="pembayaran">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 m-auto text-center col-sm-12 col-md-12">
                            <div class="banner-content content-padding">
                                <h1 class="banner-title">Pembayaran</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--MAIN HEADER AREA END -->

    <!-- PRICE AREA START  -->
    <section class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-sm-6">
                    <div class="pricing-block ">
                        <div class="price-header">
                            <!--<h4 class="price"><small>Rp</small>26</h4>-->
                            <h5>Biaya</h5>
                        </div>
                        <div class="line"></div>

                        <p>Peserta Pemakalah Luring dan Daring :IDR 500.000</p>
                        <p>Peserta Non-Pemakalah Luring :IDR 300.000 </p>
                        <p>Peserta Non-Pemakalah Daring : IDR 50.000</p>

                    </div>
                </div>
                <div class="col-lg-6 col-sm-6">
                    <div class="pricing-block ">
                        <div class="price-header">
                            <!--<h4 class="price"><small>Rp</small>26</h4>-->
                            <h5>Metode Pembayaran</h5>
                        </div>
                        <div class="line"></div>
                        <p>Bank Name : Bank Central Asia (BCA)</p>
                        <p>Swift/BIC : CENAIDJA</p>
                        <p>Account Number : 5465431358</p>
                        <p>Account Holder : Antinah Latif</p>
                        <div class="line"></div>
                    </div>
                </div>
            </div>
    </section>
    <!-- PRICE AREA END  -->
@endsection
