@extends('layouts.home')
@section('content')
    <!--MAIN BANNER AREA -->
    <div class="banner-area banner-2" id="admisi">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 m-auto text-center col-sm-12 col-md-12">
                            <div class="banner-content content-padding">
                                <h1 class="banner-title">Admisi</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--MAIN HEADER AREA END -->

    <!--  CONTACT START  -->
    <section id="contact" class="section-padding ">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-mfd-12">
                    <div class="section-heading">
                        <h4 class="section-title">Prosedur Pendaftaran</h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-sm-12 m-auto">
                    <div class="text-left">
                        <p>User fill in the registration form and click the register button</p>
                        <p>Automail will be sent to the user, then user has to open activation link sent in the email</p>
                        <p>User can login and submit abstract, payment, full paper, etc</p>
                    </div>
                    <form action="/auth/register" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-md-4 form-group">
                                <input type="text" class="form-control" name="nama_depan" placeholder="Nama Depan"
                                    required>
                                <span class="text-small text-danger mt-3 mb-3"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <input type="text" class="form-control" name="nama_tengah" placeholder="Nama Tengah">
                            </div>
                            <div class="col-md-4 form-group">
                                <input type="text" class="form-control" name="nama_belakang" placeholder="Nama Belakang"
                                    required>
                                <span class="text-small text-danger mt-3 mb-3"></span>
                            </div>
                            <div class="col-md-6 form-group">
                                <span>Role :</span>
                                <select class="form-select" name="used_role">
                                    <option value="">Pilih</option>
                                    <option value="Pengarang">Pengarang</option>
                                    <option value="Peserta">Peserta</option>
                                </select>
                                <span class="text-small text-danger mt-3 mb-3"></span>
                            </div>
                            <div class="col-md-6 form-group">
                                <span>Gender :</span>
                                <select class="form-select" name="gender">
                                    <option value="">Pilih</option>
                                    <option value="Laki-laki">Laki-laki</option>
                                    <option value="Perempuan">Perempuan</option>
                                </select>
                                <span class="text-small text-danger mt-3 mb-3"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <input type="email" class="form-control" name="email" placeholder="Email" required>
                                <span class="text-small text-danger mt-3 mb-3"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <input type="text" class="form-control" name="username" placeholder="Username" required>
                                <span class="text-small text-danger mt-3 mb-3"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <input type="password" class="form-control" name="password" placeholder="Password" required>
                                <span class="text-small text-danger mt-3 mb-3"></span>
                            </div>
                            <div class="col-12 text-center">
                                <button type="submit" class="btn btn-hero btn-circled">Daftar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
