@extends('layouts.layout', ['title' => 'Paper'])
@section('content')
    <main class="content">
        <div class="container-fluid p-0">

            <h1 class="h3 mb-3">Detail <strong>Paper</strong></h1>

            @if ($errors)
                @foreach ($errors->all() as $error)
                    <div class="badge bg-danger mb-3">{{ $error }}</div>
                @endforeach
            @endif

            <div class="row">
                <div class="col-xl-12 col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Detail Paper</h4>
                        </div>
                        <div class="card-body">
                            <div class="section-title text-primary mt-3 mb-3">Abstrak</div>
                            <div class="mb-3">
                                <label>File Abstrak</label>
                                <br>
                                <a href="" class="btn btn-info" download="Abstrak - <?= $paper['title'] ?>">Abstrak -
                                    <?= $paper['title'] ?></a>
                            </div>
                            <div class="section-title text-primary mt-3 mb-3">Paper</div>
                            <div class="mb-3">
                                <label>Kategori</label>
                                <select class="form-control" disabled>
                                    <option value="PEKM" <?= $paper['category'] == 'PEKM' ? 'selected' : '' ?>>Penguatan
                                        Ekonomi Kreatif Masyarakat</option>
                                    <option value="PTPM" <?= $paper['category'] == 'PTPM' ? 'selected' : '' ?>>Pemanfaatan
                                        Teknologi dalam Pemberdayaan Masyarakat</option>
                                    <option value="PKMTKB" <?= $paper['category'] == 'PKMTKB' ? 'selected' : '' ?>>
                                        Peningkatan Kepedulian Masyarakat Terhadap Kesenian dan Budaya</option>
                                    <option value="PKMSB" <?= $paper['category'] == 'PKMSB' ? 'selected' : '' ?>>
                                        Pembangunan Kehidupan Masyarakat Sosial dan Berpendidikan</option>
                                </select>
                            </div>
                            <div class="mb-3">
                                <label>Judul</label>
                                <input type="text" class="form-control" value="<?= $paper['title'] ?>" disabled>
                            </div>
                            <div class="mb-3">
                                <label>File Full Paper</label>
                                <br>
                                <a href="" class="btn btn-info"
                                    download="Full Paper - <?= $paper['file_full_paper'] ?>">Full Paper -
                                    <?= $paper['judul'] ?></a>
                            </div>
                            <div class="mb-3">
                                <label>Abstrak</label>
                                <textarea class="form-control" disabled><?= $paper['abstract'] ?></textarea>
                            </div>
                            <div class="mb-3">
                                <label>Kata Kunci</label>
                                <input type="text" class="form-control" value="<?= $paper['key_word'] ?>" disabled>
                            </div>
                            <div class="mb-3">
                                <label>Pengarang Bersangkutan</label>
                                <input type="text" class="form-control" value="<?= $paper['author_concerned'] ?>"
                                    disabled>
                            </div>
                            <div class="mb-3">
                                <label>File Statement of Originality</label>
                                <br>
                                <a href="" class="btn btn-info"
                                    download="Statement of Originality - <?= $paper['file_statement_of_originality'] ?>">Statement
                                    of Originality - <?= $paper['judul'] ?></a>
                            </div>
                            <div class="mb-3">
                                <label>Konfirmasi Kehadiran</label>
                                <input type="text" class="form-control"
                                    value="<?= $paper['confirmation_of_attendance'] ?>" disabled>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header">
                            <h4>Metadata Paper</h4>
                        </div>
                        <div class="card-body">
                            <div class="section-title text-primary mt-3 mb-3">Pengarang 1</div>
                            <div class="row">
                                <div class="col-4">
                                    <div class="mb-3">
                                        <label>Nama Depan</label>
                                        <input type="text" class="form-control"
                                            value="{{ $paper->paper_metadatas->first_name_author1 }}" disabled>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="mb-3">
                                        <label>Nama Tengah</label>
                                        <input type="text" class="form-control"
                                            value="{{ $paper->paper_metadatas->middle_name_author1 }}" disabled>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="mb-3">
                                        <label>Nama Belakang</label>
                                        <input type="text" class="form-control"
                                            value="{{ $paper->paper_metadatas->last_name_author1 }}" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-3">
                                <label>E-Mail</label>
                                <input type="text" class="form-control"
                                    value="{{ $paper->paper_metadatas->email_author1 }}" disabled>
                            </div>
                            <div class="mb-3">
                                <label>Institusi</label>
                                <input type="text" class="form-control"
                                    value="{{ $paper->paper_metadatas->institution_author1 }}" disabled>
                            </div>
                            <div class="mb-3">
                                <label>Pernyataan Bio</label>
                                <input type="text" class="form-control"
                                    value="{{ $paper->paper_metadatas->author_bio_statement1 }}" disabled>
                            </div>
                            <div class="section-title text-primary mt-3 mb-3">Pengarang 2</div>
                            <div class="row">
                                <div class="col-4">
                                    <div class="mb-3">
                                        <label>Nama Depan</label>
                                        <input type="text" class="form-control"
                                            value="{{ $paper->paper_metadatas->first_name_author2 }}" disabled>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="mb-3">
                                        <label>Nama Tengah</label>
                                        <input type="text" class="form-control"
                                            value="{{ $paper->paper_metadatas->middle_name_author2 }}" disabled>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="mb-3">
                                        <label>Nama Belakang</label>
                                        <input type="text" class="form-control"
                                            value="{{ $paper->paper_metadatas->last_name_author2 }}" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-3">
                                <label>E-Mail</label>
                                <input type="text" class="form-control"
                                    value="{{ $paper->paper_metadatas->email_author2 }}" disabled>
                            </div>
                            <div class="mb-3">
                                <label>Institusi</label>
                                <input type="text" class="form-control"
                                    value="{{ $paper->paper_metadatas->institution_author2 }}" disabled>
                            </div>
                            <div class="mb-3">
                                <label>Pernyataan Bio</label>
                                <input type="text" class="form-control"
                                    value="{{ $paper->paper_metadatas->author_bio_statement2 }}" disabled>
                            </div>
                            <div class="section-title text-primary mt-3 mb-3">Pengarang 3</div>
                            <div class="row">
                                <div class="col-4">
                                    <div class="mb-3">
                                        <label>Nama Depan</label>
                                        <input type="text" class="form-control"
                                            value="{{ $paper->paper_metadatas->first_name_author3 }}" disabled>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="mb-3">
                                        <label>Nama Tengah</label>
                                        <input type="text" class="form-control"
                                            value="{{ $paper->paper_metadatas->middle_name_author3 }}" disabled>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="mb-3">
                                        <label>Nama Belakang</label>
                                        <input type="text" class="form-control"
                                            value="{{ $paper->paper_metadatas->last_name_author3 }}" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-3">
                                <label>E-Mail</label>
                                <input type="text" class="form-control"
                                    value="{{ $paper->paper_metadatas->email_author3 }}" disabled>
                            </div>
                            <div class="mb-3">
                                <label>Institusi</label>
                                <input type="text" class="form-control"
                                    value="{{ $paper->paper_metadatas->institution_author3 }}" disabled>
                            </div>
                            <div class="mb-3">
                                <label>Pernyataan Bio</label>
                                <input type="text" class="form-control"
                                    value="{{ $paper->paper_metadatas->author_bio_statement3 }}" disabled>
                            </div>
                            <div class="section-title text-primary mt-3 mb-3">Pengarang 4</div>
                            <div class="row">
                                <div class="col-4">
                                    <div class="mb-3">
                                        <label>Nama Depan</label>
                                        <input type="text" class="form-control"
                                            value="{{ $paper->paper_metadatas->first_name_author4 }}" disabled>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="mb-3">
                                        <label>Nama Tengah</label>
                                        <input type="text" class="form-control"
                                            value="{{ $paper->paper_metadatas->middle_name_author4 }}" disabled>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="mb-3">
                                        <label>Nama Belakang</label>
                                        <input type="text" class="form-control"
                                            value="{{ $paper->paper_metadatas->last_name_author4 }}" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-3">
                                <label>E-Mail</label>
                                <input type="text" class="form-control"
                                    value="{{ $paper->paper_metadatas->email_author4 }}" disabled>
                            </div>
                            <div class="mb-3">
                                <label>Institusi</label>
                                <input type="text" class="form-control"
                                    value="{{ $paper->paper_metadatas->institution_author4 }}" disabled>
                            </div>
                            <div class="mb-3">
                                <label>Pernyataan Bio</label>
                                <input type="text" class="form-control"
                                    value="{{ $paper->paper_metadatas->author_bio_statement4 }}" disabled>
                            </div>
                            <div class="section-title text-primary mt-3 mb-3">Pengarang 5</div>
                            <div class="row">
                                <div class="col-4">
                                    <div class="mb-3">
                                        <label>Nama Depan</label>
                                        <input type="text" class="form-control"
                                            value="{{ $paper->paper_metadatas->first_name_author5 }}" disabled>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="mb-3">
                                        <label>Nama Tengah</label>
                                        <input type="text" class="form-control"
                                            value="{{ $paper->paper_metadatas->middle_name_author5 }}" disabled>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="mb-3">
                                        <label>Nama Belakang</label>
                                        <input type="text" class="form-control"
                                            value="{{ $paper->paper_metadatas->last_name_author5 }}" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-3">
                                <label>E-Mail</label>
                                <input type="text" class="form-control"
                                    value="{{ $paper->paper_metadatas->email_author5 }}" disabled>
                            </div>
                            <div class="mb-3">
                                <label>Institusi</label>
                                <input type="text" class="form-control"
                                    value="{{ $paper->paper_metadatas->institution_author5 }}" disabled>
                            </div>
                            <div class="mb-3">
                                <label>Pernyataan Bio</label>
                                <input type="text" class="form-control"
                                    value="{{ $paper->paper_metadatas->author_bio_statement5 }}" disabled>
                            </div>
                            <div class="section-title text-primary mt-3 mb-3">Pengarang 6</div>
                            <div class="row">
                                <div class="col-4">
                                    <div class="mb-3">
                                        <label>Nama Depan</label>
                                        <input type="text" class="form-control"
                                            value="{{ $paper->paper_metadatas->first_name_author6 }}" disabled>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="mb-3">
                                        <label>Nama Tengah</label>
                                        <input type="text" class="form-control"
                                            value="{{ $paper->paper_metadatas->middle_name_author6 }}" disabled>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="mb-3">
                                        <label>Nama Belakang</label>
                                        <input type="text" class="form-control"
                                            value="{{ $paper->paper_metadatas->last_name_author6 }}" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-3">
                                <label>E-Mail</label>
                                <input type="text" class="form-control"
                                    value="{{ $paper->paper_metadatas->email_author6 }}" disabled>
                            </div>
                            <div class="mb-3">
                                <label>Institusi</label>
                                <input type="text" class="form-control"
                                    value="{{ $paper->paper_metadatas->institution_author6 }}" disabled>
                            </div>
                            <div class="mb-3">
                                <label>Pernyataan Bio</label>
                                <input type="text" class="form-control"
                                    value="{{ $paper->paper_metadatas->author_bio_statement6 }}" disabled>
                            </div>
                            <div class="section-title text-primary mt-3 mb-3">Pengarang 7</div>
                            <div class="row">
                                <div class="col-4">
                                    <div class="mb-3">
                                        <label>Nama Depan</label>
                                        <input type="text" class="form-control"
                                            value="{{ $paper->paper_metadatas->first_name_author7 }}" disabled>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="mb-3">
                                        <label>Nama Tengah</label>
                                        <input type="text" class="form-control"
                                            value="{{ $paper->paper_metadatas->middle_name_author7 }}" disabled>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="mb-3">
                                        <label>Nama Belakang</label>
                                        <input type="text" class="form-control"
                                            value="{{ $paper->paper_metadatas->last_name_author7 }}" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-3">
                                <label>E-Mail</label>
                                <input type="text" class="form-control"
                                    value="{{ $paper->paper_metadatas->email_author7 }}" disabled>
                            </div>
                            <div class="mb-3">
                                <label>Institusi</label>
                                <input type="text" class="form-control"
                                    value="{{ $paper->paper_metadatas->institution_author7 }}" disabled>
                            </div>
                            <div class="mb-3">
                                <label>Pernyataan Bio</label>
                                <input type="text" class="form-control"
                                    value="{{ $paper->paper_metadatas->author_bio_statement7 }}" disabled>
                            </div>
                        </div>
                        @if ($paper->status == 'Sedang diproses')
                            <div class="card-footer text-right">
                                <form action="/admin/terima_abstrak" method="post" enctype="multipart/form-data"
                                    class="d-inline">
                                    <input type="hidden" class="form-control" name="id"
                                        value="<?= $paper['id'] ?>">
                                    <input type="hidden" class="form-control" name="id_pengarang"
                                        value="<?= $paper['author_id'] ?>">
                                    <div class="mb-3">
                                        <label>File LOA</label>
                                        <input type="file" class="form-control" name="file_loa"
                                            accept=".doc,.docx,application/msword,application/pdf,application/vnd.ms-excel">

                                        <span class="text-danger small">
                                            @error('file_loa')
                                                {{ $message }}
                                            @enderror
                                    </div>
                                    <button type="submit" class="btn btn-primary"
                                        onclick="return confirm('Terima abstrak?')">Terima</button>
                                </form>
                                <form action="/admin/tolak_abstrak" method="post" class="d-inline">
                                    <input type="hidden" class="form-control" name="id"
                                        value="<?= $paper['id'] ?>">
                                    <input type="hidden" class="form-control" name="id_pengarang"
                                        value="<?= $paper['id_pengarang'] ?>">
                                    <button type="submit" class="btn btn-danger"
                                        onclick="return confirm('Tolak abstrak?')">Tolak</button>
                                </form>
                            </div>
                        @endif
                    </div>

                    <div class="card">
                        <div class="card-header">
                            <h4>Video Paper</h4>
                        </div>
                        <div class="card-body">
                            <div class="mb-3">
                                <label>Link Video</label>
                                <input type="text" class="form-control" value="<?= $paper['link_video'] ?>" disabled>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header">
                            <h4>File Tambahan Paper</h4>
                        </div>
                        <div class="card-body">
                            <div class="mb-3">
                                <label>File Tambahan</label>
                                <br>
                                @if ($paper->supplementary_file)
                                    <a href="" class="btn btn-info"
                                        download="File Tambahan - {{ $paper->title }}">File Tambahan -
                                        {{ $paper->title }}</a>
                                @endif
                            </div>
                        </div>
                        @if ($paper->reviewer_id == 0)
                            <div class="card-footer text-right">
                                <form action="{{ route('update.reviewer', $paper->id) }}" method="post"
                                    class="d-inline">
                                    @csrf
                                    <input type="hidden" class="form-control" name="id"
                                        value="<?= $paper['id'] ?>">
                                    <input type="hidden" class="form-control" name="author_id"
                                        value="<?= $paper['author_id'] ?>">
                                    <input type="hidden" class="form-control" name="category"
                                        value="<?= $paper['category'] ?>">
                                    <div class="mb-3">
                                        <label>Pengulas</label>
                                        <select class="form-control" name="reviewer_id">
                                            <option value="">Pilih</option>
                                            @foreach ($reviewers as $reviewer)
                                                <option value="{{ $reviewer->id }}">{{ $reviewer->full_name }}</option>
                                            @endforeach
                                        </select>
                                        <span class="text-small text-danger mt-3 mb-3">
                                            @error('reviewer_id')
                                                {{ $message }}
                                            @enderror
                                        </span>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Pilih</button>
                                </form>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
