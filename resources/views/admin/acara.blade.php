@extends('layouts.layout', ['title' => 'Acara'])
@section('content')
    <main class="content">
        <div class="container-fluid p-0">

            <h1 class="h3 mb-3"><strong>Acara</strong></h1>

            <div class="row">
                <div class="col-xl-12 d-flex">
                    <div class="w-100">
                        <div class="row">
                            <div class="col-6">
                                <div class="card">
                                    <div class="card-header">
                                        <h5 class="card-title">Jadwal Acara</h5>
                                    </div>
                                    <div class="card-body">
                                        <p>Tanggal: <strong></strong></p>
                                        <p>Waktu: <strong></strong></p>
                                        <p>Zoom Link: <strong></strong></p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </main>
@endsection
