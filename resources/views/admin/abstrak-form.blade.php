@extends('layouts.layout', ['title' => 'Abstrak'])
@section('content')
    <main class="content">
        <div class="container-fluid p-0">

            <h1 class="h3 mb-3"><strong>Buat abstrak</strong></h1>

            @if (session('success'))
                <div class="badge bg-success mb-3">{{ session('success') }}</div>
            @endif

            @if (session('danger'))
                <div class="badge bg-danger mb-3">{{ session('danger') }}</div>
            @endif

            <div class="row">
                <div class="col-xl-12 col-xxl-12">
                    <form action="{{ route('store.abstract') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="card">
                            <div class="card-header">
                                <h4>Abstrak</h4>
                            </div>
                            <div class="card-body">
                                <div class="mb-3">
                                    <label class="form-label">File Abstrak</label>
                                    <input type="file" class="form-control" name="file_abstrak"
                                        accept=".doc,.docx,application/msword" value="{{ old('file_abstrak') }}">
                                    <span class="text-small text-danger mt-3 mb-3"> @error('file_abstrak')
                                            {{ $message }}
                                        @enderror
                                    </span>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Kategori</label>
                                    <select class="form-select" name="kategori">
                                        <option value="">Pilih kategori</option>
                                        <option value="PEKM" {{ old('kategori') == 'PEKM' ? 'selected' : '' }}>Penguatan
                                            Ekonomi Kreatif Masyarakat</option>
                                        <option value="PTPM" {{ old('kategori') == 'PTPM' ? 'selected' : '' }}>Pemanfaatan
                                            Teknologi dalam Pemberdayaan Masyarakat</option>
                                        <option value="PKMTKB" {{ old('kategori') == 'PKMTKB' ? 'selected' : '' }}>
                                            Peningkatan
                                            Kepedulian Masyarakat Terhadap Kesenian dan
                                            Budaya</option>
                                        <option value="PKMSB" {{ old('kategori') == 'PKMSB' ? 'selected' : '' }}>
                                            Pembangunan
                                            Kehidupan Masyarakat Sosial dan Berpendidikan
                                        </option>
                                    </select>
                                    <span class="text-small text-danger mt-3 mb-3">
                                        @error('kategori')
                                            {{ $message }}
                                        @enderror
                                    </span>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Judul</label>
                                    <input type="text" class="form-control" name="judul" value="{{ old('judul') }}">
                                    <span class="text-small text-danger mt-3 mb-3">
                                        @error('judul')
                                            {{ $message }}
                                        @enderror
                                    </span>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Abstrak</label>
                                    <textarea class="form-control" name="abstrak">{{ old('abstrak') }}</textarea>
                                    <div class="text-small text-danger mt-3 mb-3">
                                        @error('abstrak')
                                            {{ $message }}
                                        @enderror
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Kata Kunci</label>
                                    <input type="text" class="form-control" name="kata_kunci"
                                        value="{{ old('kata_kunci') }}" value="{{ old('kata_kunci') }}">
                                    <div class="text-small text-danger mt-3 mb-3">
                                        @error('kata_kunci')
                                            {{ $message }}
                                        @enderror
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Pengarang Bersangkutan</label>
                                    <input type="text" class="form-control" name="pengarang_bersangkutan"
                                        value="{{ old('pengarang_bersangkutan') }}">
                                    <div class="text-small text-danger mt-3 mb-3">
                                        @error('pengarang_bersangkutan')
                                            {{ $message }}
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header">
                                <h4>Metadata</h4>
                            </div>
                            <div class="card-body">
                                <div class="section-title text-primary mt-3 mb-3">Pengarang 1</div>
                                <div class="row">
                                    <div class="col-4">
                                        <div class="mb-3">
                                            <label class="form-label">Nama Depan</label>
                                            <input type="text" class="form-control" name="nama_depan_pengarang1"
                                                value="{{ old('nama_depan_pengarang1') }}">
                                            <div class="text-small text-danger mt-3 mb-3">
                                                @error('nama_depan_pengarang1')
                                                    {{ $message }}
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="mb-3">
                                            <label class="form-label">Nama Tengah</label>
                                            <input type="text" class="form-control" name="nama_tengah_pengarang1"
                                                value="{{ old('nama_tengah_pengarang1') }}">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="mb-3">
                                            <label class="form-label">Nama Belakang</label>
                                            <input type="text" class="form-control" name="nama_belakang_pengarang1"
                                                value="{{ old('nama_belakang_pengarang1') }}">
                                            <div class="text-small text-danger mt-3 mb-3">
                                                @error('nama_belakang_pengarang1')
                                                    {{ $message }}
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">E-Mail</label>
                                    <input type="text" class="form-control" name="email_pengarang1"
                                        value="{{ old('email_pengarang1') }}">
                                    <div class="text-small text-danger mt-3 mb-3">
                                        @error('email_pengarang1')
                                            {{ $message }}
                                        @enderror
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Institusi</label>
                                    <input type="text" class="form-control " name="institusi_pengarang1"
                                        value="{{ old('institusi_pengarang1') }}">
                                    <div class="text-small text-danger mt-3 mb-3">
                                        @error('institusi_pengarang1')
                                            {{ $message }}
                                        @enderror
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Pernyataan Bio</label>
                                    <input type="text" class="form-control" name="pernyataan_bio_pengarang1"
                                        value="{{ old('pernyataan_bio_pengarang1') }}">
                                    <div class="text-small text-danger mt-3 mb-3">
                                        @error('pernyataan_bio_pengarang1')
                                            {{ $message }}
                                        @enderror
                                    </div>
                                </div>
                                <div class="section-title text-primary mt-3 mb-3">Pengarang 2</div>
                                <div class="row">
                                    <div class="col-4">
                                        <div class="mb-3">
                                            <label class="form-label">Nama Depan</label>
                                            <input type="text" class="form-control" name="nama_depan_pengarang2"
                                                value="">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="mb-3">
                                            <label class="form-label">Nama Tengah</label>
                                            <input type="text" class="form-control" name="nama_tengah_pengarang2"
                                                value="">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="mb-3">
                                            <label class="form-label">Nama Belakang</label>
                                            <input type="text" class="form-control" name="nama_belakang_pengarang2"
                                                value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">E-Mail</label>
                                    <input type="text" class="form-control" name="email_pengarang2" value="">
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Institusi</label>
                                    <input type="text" class="form-control" name="institusi_pengarang2"
                                        value="">
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Pernyataan Bio</label>
                                    <input type="text" class="form-control" name="pernyataan_bio_pengarang2"
                                        value="">
                                </div>
                                <div class="section-title text-primary mt-3 mb-3">Pengarang 3</div>
                                <div class="row">
                                    <div class="col-4">
                                        <div class="mb-3">
                                            <label class="form-label">Nama Depan</label>
                                            <input type="text" class="form-control" name="nama_depan_pengarang3"
                                                value="">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="mb-3">
                                            <label class="form-label">Nama Tengah</label>
                                            <input type="text" class="form-control" name="nama_tengah_pengarang3"
                                                value="">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="mb-3">
                                            <label class="form-label">Nama Belakang</label>
                                            <input type="text" class="form-control" name="nama_belakang_pengarang3"
                                                value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">E-Mail</label>
                                    <input type="text" class="form-control" name="email_pengarang3" value="">
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Institusi</label>
                                    <input type="text" class="form-control" name="institusi_pengarang3"
                                        value="">
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Pernyataan Bio</label>
                                    <input type="text" class="form-control" name="pernyataan_bio_pengarang3"
                                        value="">
                                </div>
                                <div class="section-title text-primary mt-3 mb-3">Pengarang 4</div>
                                <div class="row">
                                    <div class="col-4">
                                        <div class="mb-3">
                                            <label class="form-label">Nama Depan</label>
                                            <input type="text" class="form-control" name="nama_depan_pengarang4"
                                                value="">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="mb-3">
                                            <label class="form-label">Nama Tengah</label>
                                            <input type="text" class="form-control" name="nama_tengah_pengarang4"
                                                value="">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="mb-3">
                                            <label class="form-label">Nama Belakang</label>
                                            <input type="text" class="form-control" name="nama_belakang_pengarang4"
                                                value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">E-Mail</label>
                                    <input type="text" class="form-control" name="email_pengarang4" value="">
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Institusi</label>
                                    <input type="text" class="form-control" name="institusi_pengarang4"
                                        value="">
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Pernyataan Bio</label>
                                    <input type="text" class="form-control" name="pernyataan_bio_pengarang4"
                                        value="">
                                </div>
                                <div class="section-title text-primary mt-3 mb-3">Pengarang 5</div>
                                <div class="row">
                                    <div class="col-4">
                                        <div class="mb-3">
                                            <label class="form-label">Nama Depan</label>
                                            <input type="text" class="form-control" name="nama_depan_pengarang5"
                                                value="">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="mb-3">
                                            <label class="form-label">Nama Tengah</label>
                                            <input type="text" class="form-control" name="nama_tengah_pengarang5"
                                                value="">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="mb-3">
                                            <label class="form-label">Nama Belakang</label>
                                            <input type="text" class="form-control" name="nama_belakang_pengarang5"
                                                value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">E-Mail</label>
                                    <input type="text" class="form-control" name="email_pengarang5" value="">
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Institusi</label>
                                    <input type="text" class="form-control" name="institusi_pengarang5"
                                        value="">
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Pernyataan Bio</label>
                                    <input type="text" class="form-control" name="pernyataan_bio_pengarang5"
                                        value="">
                                </div>
                                <div class="section-title text-primary mt-3 mb-3">Pengarang 6</div>
                                <div class="row">
                                    <div class="col-4">
                                        <div class="mb-3">
                                            <label class="form-label">Nama Depan</label>
                                            <input type="text" class="form-control" name="nama_depan_pengarang6"
                                                value="">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="mb-3">
                                            <label class="form-label">Nama Tengah</label>
                                            <input type="text" class="form-control" name="nama_tengah_pengarang6"
                                                value="">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="mb-3">
                                            <label class="form-label">Nama Belakang</label>
                                            <input type="text" class="form-control" name="nama_belakang_pengarang6"
                                                value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">E-Mail</label>
                                    <input type="text" class="form-control" name="email_pengarang6" value="">
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Institusi</label>
                                    <input type="text" class="form-control" name="institusi_pengarang6"
                                        value="">
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Pernyataan Bio</label>
                                    <input type="text" class="form-control" name="pernyataan_bio_pengarang6"
                                        value="">
                                </div>
                                <div class="section-title text-primary mt-3 mb-3">Pengarang 7</div>
                                <div class="row">
                                    <div class="col-4">
                                        <div class="mb-3">
                                            <label class="form-label">Nama Depan</label>
                                            <input type="text" class="form-control" name="nama_depan_pengarang7"
                                                value="">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="mb-3">
                                            <label class="form-label">Nama Tengah</label>
                                            <input type="text" class="form-control" name="nama_tengah_pengarang7"
                                                value="">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="mb-3">
                                            <label class="form-label">Nama Belakang</label>
                                            <input type="text" class="form-control" name="nama_belakang_pengarang7"
                                                value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">E-Mail</label>
                                    <input type="text" class="form-control" name="email_pengarang7" value="">
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Institusi</label>
                                    <input type="text" class="form-control" name="institusi_pengarang7"
                                        value="">
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Pernyataan Bio</label>
                                    <input type="text" class="form-control" name="pernyataan_bio_pengarang7"
                                        value="">
                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <button class="btn btn-primary mr-1" type="submit">Submit</button>
                                <button class="btn btn-secondary" type="reset">Reset</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </main>
@endsection
