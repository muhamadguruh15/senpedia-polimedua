@extends('layouts.layout', ['title' => 'Pengaturan'])
@section('content')
    <main class="content">
        <div class="container-fluid p-0">

            <div class="mb-3">
                <h1 class="h3 d-inline align-middle">Profil</h1>
            </div>

            @if (session('success'))
                <div class="badge bg-success mb-3">{{ session('success') }}</div>
            @endif
            @if (session('danger'))
                <div class="badge bg-danger mb-3">{{ session('danger') }}</div>
            @endif

            <div class="row">
                <div class="col-md-8 col-xl-9">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title mb-0">Pengaturan</h5>
                        </div>
                        <form action="{{ route('settings.password') }}" method="post">
                            @csrf
                            <div class="card-body h-100">
                                <div class="mb-3">
                                    <label class="form-label">Password Lama</label>
                                    <input type="password" class="form-control" name="password_lama">
                                    @if ($errors->has('password_lama'))
                                        <span class="text-danger small">{{ $errors->first('password_lama') }}</span>
                                    @endif
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Password Baru</label>
                                    <input type="password" class="form-control" name="password_baru">
                                    @if ($errors->has('password_baru'))
                                        <span class="text-danger small">{{ $errors->first('password_baru') }}</span>
                                    @endif
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Konfirmasi Password</label>
                                    <input type="password" class="form-control" name="konfirmasi_password">
                                    @if ($errors->has('konfirmasi_password'))
                                        <span class="text-danger small">{{ $errors->first('konfirmasi_password') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Ubah</button>
                                <button type="reset" class="btn btn-secondary">Batal</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </main>
@endsection
