@extends('layouts.layout', ['title' => 'Paper'])
@section('content')
    <main class="content">
        <div class="container-fluid p-0">

            <h1 class="h3 mb-3"><strong>Paper</strong></h1>

            @if (session('success'))
                <div class="badge bg-success mb-3">{{ session('success') }}</div>
            @endif

            @if (session('danger'))
                <div class="badge bg-danger mb-3">{{ session('danger') }}</div>
            @endif

            {{-- <div class="row">
            <div class="col-xl-12 col-xxl-12">

                <?php if (!empty($paper)) :
                    if ($paper['status'] != 'Diterima') : ?>
                        <div class="alert alert-primary">Menunggu admin menerima abstrak</div>
                    <?php elseif ($paper['status'] == 'Diterima' && $userPayment['status'] != 'Dikonfirmasi') : ?>
                        <div class="alert alert-primary">Lakukan pembayaran terlebih dahulu untuk melanjutkan paper</div>
                    <?php elseif ($paper['status'] == 'Diterima' && $userPayment['status'] == 'Dikonfirmasi') : ?>
                        <div class="row">
                            <div class="col-12 col-md-12 col-lg-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4>Detail Paper</h4>
                                    </div>
                                    <div class="card-body">
                                        <?php if ($paper['id_pengulas'] != 0) : ?>
                                            <div class="card-header-action">
                                                <a href="/pengarang/ulasan_paper/<?= $paper['id'] ?>" class="btn btn-primary">Hasil Ulasan</a>
                                            </div>
                                        <?php endif; ?>
                                        <div class="section-title text-primary mt-3 mb-3">Abstrak</div>
                                        <div class="mb-3">
                                            <label>File Abstrak</label>
                                            <br>
                                            <a href="<?= base_url('files/file-abstrak/' . $paper['file_abstrak']) ?>" class="btn btn-info" download="Abstrak - <?= $paper['judul'] ?>">Abstrak - <?= $paper['judul'] ?></a>
                                        </div>
                                        <div class="section-title text-primary mt-3 mb-3">Paper</div>
                                        <div class="mb-3">
                                            <label>Kategori</label>
                                            <select class="form-control" disabled>
                                                <option value="PEKM" <?= $paper['kategori'] == 'PEKM' ? 'selected' : '' ?>>Penguatan Ekonomi Kreatif Masyarakat</option>
                                                <option value="PTPM" <?= $paper['kategori'] == 'PTPM' ? 'selected' : '' ?>>Pemanfaatan Teknologi dalam Pemberdayaan Masyarakat</option>
                                                <option value="PKMTKB" <?= $paper['kategori'] == 'PKMTKB' ? 'selected' : '' ?>>Peningkatan Kepedulian Masyarakat Terhadap Kesenian dan Budaya</option>
                                                <option value="PKMSB" <?= $paper['kategori'] == 'PKMSB' ? 'selected' : '' ?>>Pembangunan Kehidupan Masyarakat Sosial dan Berpendidikan</option>
                                            </select>
                                        </div>
                                        <div class="mb-3">
                                            <label>Judul</label>
                                            <input type="text" class="form-control" value="<?= $paper['judul'] ?>" disabled>
                                        </div>
                                        <div class="mb-3">
                                            <label>File Full Paper</label>
                                            <br>
                                            <a href="<?= base_url('files/file-full-paper/' . $paper['file_full_paper']) ?>" class="btn btn-info" download="Full Paper - <?= $paper['file_full_paper'] ?>">Full Paper - <?= $paper['judul'] ?></a>
                                        </div>
                                        <div class="mb-3">
                                            <label>Abstrak</label>
                                            <textarea class="form-control" disabled><?= $paper['abstrak'] ?></textarea>
                                        </div>
                                        <div class="mb-3">
                                            <label>Kata Kunci</label>
                                            <input type="text" class="form-control" value="<?= $paper['kata_kunci'] ?>" disabled>
                                        </div>
                                        <div class="mb-3">
                                            <label>Pengarang Bersangkutan</label>
                                            <input type="text" class="form-control" value="<?= $paper['pengarang_bersangkutan'] ?>" disabled>
                                        </div>
                                        <div class="mb-3">
                                            <label>File Statement of Originality</label>
                                            <br>
                                            <a href="<?= base_url('files/file-statement-of-originality/' . $paper['file_statement_of_originality']) ?>" class="btn btn-info" download="Statement of Originality - <?= $paper['file_statement_of_originality'] ?>">Statement of Originality - <?= $paper['judul'] ?></a>
                                        </div>
                                        <div class="mb-3">
                                            <label>Konfirmasi Kehadiran</label>
                                            <input type="text" class="form-control" value="<?= $paper['konfirmasi_kehadiran'] ?>" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <h4>Metadata Paper</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="section-title text-primary mt-3 mb-3">Pengarang 1</div>
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="mb-3">
                                                    <label>Nama Depan</label>
                                                    <input type="text" class="form-control" value="<?= $paper['nama_depan_pengarang1'] ?>" disabled>
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="mb-3">
                                                    <label>Nama Tengah</label>
                                                    <input type="text" class="form-control" value="<?= $paper['nama_tengah_pengarang1'] ?>" disabled>
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="mb-3">
                                                    <label>Nama Belakang</label>
                                                    <input type="text" class="form-control" value="<?= $paper['nama_belakang_pengarang1'] ?>" disabled>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mb-3">
                                            <label>E-Mail</label>
                                            <input type="text" class="form-control" value="<?= $paper['email_pengarang1'] ?>" disabled>
                                        </div>
                                        <div class="mb-3">
                                            <label>Institusi</label>
                                            <input type="text" class="form-control" value="<?= $paper['institusi_pengarang1'] ?>" disabled>
                                        </div>
                                        <div class="mb-3">
                                            <label>Pernyataan Bio</label>
                                            <input type="text" class="form-control" value="<?= $paper['pernyataan_bio_pengarang1'] ?>" disabled>
                                        </div>
                                        <div class="section-title text-primary mt-3 mb-3">Pengarang 2</div>
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="mb-3">
                                                    <label>Nama Depan</label>
                                                    <input type="text" class="form-control" value="<?= $paper['nama_depan_pengarang2'] ?>" disabled>
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="mb-3">
                                                    <label>Nama Tengah</label>
                                                    <input type="text" class="form-control" value="<?= $paper['nama_tengah_pengarang2'] ?>" disabled>
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="mb-3">
                                                    <label>Nama Belakang</label>
                                                    <input type="text" class="form-control" value="<?= $paper['nama_belakang_pengarang2'] ?>" disabled>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mb-3">
                                            <label>E-Mail</label>
                                            <input type="text" class="form-control" value="<?= $paper['email_pengarang2'] ?>" disabled>
                                        </div>
                                        <div class="mb-3">
                                            <label>Institusi</label>
                                            <input type="text" class="form-control" value="<?= $paper['institusi_pengarang2'] ?>" disabled>
                                        </div>
                                        <div class="mb-3">
                                            <label>Pernyataan Bio</label>
                                            <input type="text" class="form-control" value="<?= $paper['pernyataan_bio_pengarang2'] ?>" disabled>
                                        </div>
                                        <div class="section-title text-primary mt-3 mb-3">Pengarang 3</div>
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="mb-3">
                                                    <label>Nama Depan</label>
                                                    <input type="text" class="form-control" value="<?= $paper['nama_depan_pengarang3'] ?>" disabled>
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="mb-3">
                                                    <label>Nama Tengah</label>
                                                    <input type="text" class="form-control" value="<?= $paper['nama_tengah_pengarang3'] ?>" disabled>
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="mb-3">
                                                    <label>Nama Belakang</label>
                                                    <input type="text" class="form-control" value="<?= $paper['nama_belakang_pengarang3'] ?>" disabled>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mb-3">
                                            <label>E-Mail</label>
                                            <input type="text" class="form-control" value="<?= $paper['email_pengarang3'] ?>" disabled>
                                        </div>
                                        <div class="mb-3">
                                            <label>Institusi</label>
                                            <input type="text" class="form-control" value="<?= $paper['institusi_pengarang3'] ?>" disabled>
                                        </div>
                                        <div class="mb-3">
                                            <label>Pernyataan Bio</label>
                                            <input type="text" class="form-control" value="<?= $paper['pernyataan_bio_pengarang3'] ?>" disabled>
                                        </div>
                                        <div class="section-title text-primary mt-3 mb-3">Pengarang 4</div>
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="mb-3">
                                                    <label>Nama Depan</label>
                                                    <input type="text" class="form-control" value="<?= $paper['nama_depan_pengarang4'] ?>" disabled>
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="mb-3">
                                                    <label>Nama Tengah</label>
                                                    <input type="text" class="form-control" value="<?= $paper['nama_tengah_pengarang4'] ?>" disabled>
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="mb-3">
                                                    <label>Nama Belakang</label>
                                                    <input type="text" class="form-control" value="<?= $paper['nama_belakang_pengarang4'] ?>" disabled>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mb-3">
                                            <label>E-Mail</label>
                                            <input type="text" class="form-control" value="<?= $paper['email_pengarang4'] ?>" disabled>
                                        </div>
                                        <div class="mb-3">
                                            <label>Institusi</label>
                                            <input type="text" class="form-control" value="<?= $paper['institusi_pengarang4'] ?>" disabled>
                                        </div>
                                        <div class="mb-3">
                                            <label>Pernyataan Bio</label>
                                            <input type="text" class="form-control" value="<?= $paper['pernyataan_bio_pengarang4'] ?>" disabled>
                                        </div>
                                        <div class="section-title text-primary mt-3 mb-3">Pengarang 5</div>
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="mb-3">
                                                    <label>Nama Depan</label>
                                                    <input type="text" class="form-control" value="<?= $paper['nama_depan_pengarang5'] ?>" disabled>
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="mb-3">
                                                    <label>Nama Tengah</label>
                                                    <input type="text" class="form-control" value="<?= $paper['nama_tengah_pengarang5'] ?>" disabled>
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="mb-3">
                                                    <label>Nama Belakang</label>
                                                    <input type="text" class="form-control" value="<?= $paper['nama_belakang_pengarang5'] ?>" disabled>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mb-3">
                                            <label>E-Mail</label>
                                            <input type="text" class="form-control" value="<?= $paper['email_pengarang5'] ?>" disabled>
                                        </div>
                                        <div class="mb-3">
                                            <label>Institusi</label>
                                            <input type="text" class="form-control" value="<?= $paper['institusi_pengarang5'] ?>" disabled>
                                        </div>
                                        <div class="mb-3">
                                            <label>Pernyataan Bio</label>
                                            <input type="text" class="form-control" value="<?= $paper['pernyataan_bio_pengarang5'] ?>" disabled>
                                        </div>
                                        <div class="section-title text-primary mt-3 mb-3">Pengarang 6</div>
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="mb-3">
                                                    <label>Nama Depan</label>
                                                    <input type="text" class="form-control" value="<?= $paper['nama_depan_pengarang6'] ?>" disabled>
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="mb-3">
                                                    <label>Nama Tengah</label>
                                                    <input type="text" class="form-control" value="<?= $paper['nama_tengah_pengarang6'] ?>" disabled>
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="mb-3">
                                                    <label>Nama Belakang</label>
                                                    <input type="text" class="form-control" value="<?= $paper['nama_belakang_pengarang6'] ?>" disabled>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mb-3">
                                            <label>E-Mail</label>
                                            <input type="text" class="form-control" value="<?= $paper['email_pengarang6'] ?>" disabled>
                                        </div>
                                        <div class="mb-3">
                                            <label>Institusi</label>
                                            <input type="text" class="form-control" value="<?= $paper['institusi_pengarang6'] ?>" disabled>
                                        </div>
                                        <div class="mb-3">
                                            <label>Pernyataan Bio</label>
                                            <input type="text" class="form-control" value="<?= $paper['pernyataan_bio_pengarang6'] ?>" disabled>
                                        </div>
                                        <div class="section-title text-primary mt-3 mb-3">Pengarang 7</div>
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="mb-3">
                                                    <label>Nama Depan</label>
                                                    <input type="text" class="form-control" value="<?= $paper['nama_depan_pengarang7'] ?>" disabled>
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="mb-3">
                                                    <label>Nama Tengah</label>
                                                    <input type="text" class="form-control" value="<?= $paper['nama_tengah_pengarang7'] ?>" disabled>
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="mb-3">
                                                    <label>Nama Belakang</label>
                                                    <input type="text" class="form-control" value="<?= $paper['nama_belakang_pengarang7'] ?>" disabled>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mb-3">
                                            <label>E-Mail</label>
                                            <input type="text" class="form-control" value="<?= $paper['email_pengarang7'] ?>" disabled>
                                        </div>
                                        <div class="mb-3">
                                            <label>Institusi</label>
                                            <input type="text" class="form-control" value="<?= $paper['institusi_pengarang7'] ?>" disabled>
                                        </div>
                                        <div class="mb-3">
                                            <label>Pernyataan Bio</label>
                                            <input type="text" class="form-control" value="<?= $paper['pernyataan_bio_pengarang7'] ?>" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <h4>Video Paper</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="mb-3">
                                            <label>Link Video</label>

                                            <input type="text" class="form-control" value="<?= $paper['link_video'] ?>" disabled>
                                             <small><span class="text-danger">* (Ketentuan upload video, silahkan unduh pada link <a href=www.google.com>ini </a>)</span></small>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <h4>File Tambahan Paper</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="mb-3">
                                            <label>File Tambahan</label>
                                            <br>
                                            <a href="<?= base_url('files/file-tambahan/' . $paper['file_tambahan']) ?>" class="btn btn-info" download="File Tambahan - <?= $paper['judul'] ?>">File Tambahan - <?= $paper['judul'] ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                <?php endif;
                endif; ?>
            </div>
        </div> --}}

        </div>
    </main>
@endsection
