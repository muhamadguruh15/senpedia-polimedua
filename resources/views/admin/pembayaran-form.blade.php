@extends('layouts.layout', ['title' => 'Pembayaran'])
@section('content')
    <main class="content">
        <div class="container-fluid p-0">

            <h1 class="h3 mb-3"><strong>Pembayaran</strong></h1>

            @if (session('success'))
                <div class="badge bg-success mb-3">{{ session('success') }}</div>
            @endif

            @if (session('danger'))
                <div class="badge bg-danger mb-3">{{ session('danger') }}</div>
            @endif

            <div class="row">
                <div class="col-xl-12 d-flex">
                    <div class="w-100">
                        <div class="row">
                            <div class="col-md-4 col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5 class="card-title">Biaya</h5>
                                    </div>
                                    <div class="card-body">
                                        <p>Peserta Pemakalah Luring dan Daring: <strong>IDR 500.000</strong></p>
                                        <p>Peserta Non-Pemakalah Luring: <strong>IDR 300.000</strong></p>
                                        <p>Peserta Non-Pemakalah Daring: <strong>IDR 50.000</strong></p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-8 col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5 class="card-title">Transfer Bank</h5>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-4">
                                                <p>Transfer Bank Name: <strong>Bank Central Asia (BCA)</strong></p>
                                                <p>Swift/ BIC: <strong>Swift/BIC : CENAIDJA</strong></p>
                                                <p>Account Number: <strong>5465431358</strong></p>
                                                <p>Account Holder: <strong>Antinah Latif</strong></p>
                                            </div>
                                            <div class="col-8">
                                                @if (!empty($abstrak) && $abstrak['status'] == 'Diterima')
                                                    @if ($userInfo['active'] == 0)
                                                        <div class="badge bg-warning mb-3">Silakan verifikasi alamat email
                                                            anda terlebih dahulu</div>
                                                    @else
                                                        <p>Status:
                                                            <span
                                                                class="badge bg-{{ $pembayaran['status'] == 'Dikonfirmasi' ? 'success' : ($pembayaran['status'] == 'Ditolak' ? 'danger' : ($pembayaran['status'] == 'Menunggu konfirmasi' ? 'primary' : 'secondary')) }}">
                                                                {{ $pembayaran['status'] }}
                                                            </span>
                                                        </p>
                                                        @if ($pembayaran['proof_of_payment'] != null)
                                                            <img src="{{ Storage::url('images/bukti-pembayaran/' . $pembayaran->proof_of_payment) }}"
                                                                alt="Bukti pembayaran"
                                                                style="height: 240px; width:100%; object-fit: cover; object-position: center;">
                                                        @endif

                                                        @if ($pembayaran['status'] != 'Dikonfirmasi')
                                                            <form action="{{ route('upload.payment') }}" method="post"
                                                                enctype="multipart/form-data">
                                                                @csrf
                                                                <div class="form-group mt-4 mb-4">
                                                                    <input type="hidden" class="form-control"
                                                                        name="id" value="{{ $pembayaran['id'] }}">
                                                                    <input type="hidden" class="form-control"
                                                                        name="bukti_pembayaran_lama"
                                                                        value="{{ $pembayaran['proof_of_payment'] }}">
                                                                    <label>File</label>
                                                                    <input type="file" class="form-control"
                                                                        name="bukti_pembayaran" accept=".jpg,.jpeg,.png">
                                                                    <span
                                                                        class="text-danger small">{{ $errors->first('bukti_pembayaran') }}</span>
                                                                </div>
                                                                <button class="btn btn-primary mr-1"
                                                                    type="submit">Upload</button>
                                                            </form>
                                                        @endif
                                                    @endif
                                                @endif
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </main>
@endsection
