@extends('layouts.layout', ['title' => 'Dashboard'])
@section('content')
    <main class="content">
        <div class="container-fluid p-0">

            <h1 class="h3 mb-3"><strong>Dashboard</strong></h1>

            @if (session('success'))
                <div class="badge bg-success mb-3">{{ session('success') }}</div>
            @endif

            @if (session('danger'))
                <div class="badge bg-danger mb-3">{{ session('danger') }}</div>
            @endif

            @if ($userInfo->user_role === 'Admin')
                <div class="row">
                    <div class="col-xl-6 col-xxl-5 d-flex">
                        <div class="w-100">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col mt-0">
                                                    <h5 class="card-title">Total Pengguna</h5>
                                                </div>
                                                <div class="col-auto">
                                                    <div class="stat text-primary">
                                                        <i class="align-middle" data-feather="users"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <h1 class="mt-1 mb-3">{{ $totalPengguna }}</h1>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col mt-0">
                                                    <h5 class="card-title">Total Abstrak</h5>
                                                </div>
                                                <div class="col-auto">
                                                    <div class="stat text-primary">
                                                        <i class="align-middle" data-feather="users"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <h1 class="mt-1 mb-3">{{ $totalAbstrak }}</h1>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col mt-0">
                                                    <h5 class="card-title">Total Paper</h5>
                                                </div>
                                                <div class="col-auto">
                                                    <div class="stat text-primary">
                                                        <i class="align-middle" data-feather="dollar-sign"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <h1 class="mt-1 mb-3">{{ $totalPaper }}</h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-6 col-xxl-7">
                        <div class="card">
                            <div class="card-header">
                                <h5 class="card-title">Periode</h5>
                                <h6 class="card-subtitle text-muted">Klik tombol untuk mengubah periode pada sistem.</h6>
                            </div>
                            <div class="card-body">
                                <p>Periode Submisi: </p>
                                <div class="btn-group mb-3" role="group" aria-label="Default button group">
                                    <form action="{{ route('update.submisi') }}" method="post">
                                        @csrf
                                        <button type="{{ $sistem?->submission == 1 ? 'reset' : 'submit' }}"
                                            class="btn btn-{{ $sistem?->submission == 1 ? 'primary' : 'secondary' }}">Berlangsung</button>
                                        <button type="{{ $sistem?->submission == 0 ? 'reset' : 'submit' }}"
                                            class="btn btn-{{ $sistem?->submission == 0 ? 'primary' : 'secondary' }}">Berakhir</button>
                                    </form>
                                </div>
                                <br>
                                <p>Periode Pengumuman: </p>
                                <div class="btn-group mb-3" role="group" aria-label="Default button group">
                                    <form action="{{ route('update.selection') }}" method="post">
                                        @csrf
                                        <button type="{{ $sistem?->selection == 1 ? 'reset' : 'submit' }}"
                                            class="btn btn-{{ $sistem?->selection == 1 ? 'primary' : 'secondary' }}">Berlangsung</button>
                                        <button type="{{ $sistem?->selection == 0 ? 'reset' : 'submit' }}"
                                            class="btn btn-{{ $sistem?->selection == 0 ? 'primary' : 'secondary' }}">Berakhir</button>
                                    </form>
                                </div>
                                <br>
                                <p>Periode Konferensi: </p>
                                <div class="btn-group mb-3" role="group" aria-label="Default button group">
                                    <form action="{{ route('update.conference') }}" method="post">
                                        @csrf
                                        <button type="{{ $sistem?->conference == 1 ? 'reset' : 'submit' }}"
                                            class="btn btn-{{ $sistem?->conference == 1 ? 'primary' : 'secondary' }}">Berlangsung</button>
                                        <button type="{{ $sistem?->conference == 0 ? 'reset' : 'submit' }}"
                                            class="btn btn-{{ $sistem?->conference == 0 ? 'primary' : 'secondary' }}">Berakhir</button>
                                    </form>
                                </div>
                                <br>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

            @if ($userInfo->user_role === 'Pengarang')
                <div class="row">
                    <div class="col-xl-6 col-xxl-5 d-flex">
                        <div class="w-100">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col mt-0">
                                                    <h5 class="card-title">Total Paper</h5>
                                                </div>

                                                <div class="col-auto">
                                                    <div class="stat text-primary">
                                                        <i class="align-middle" data-feather="dollar-sign"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <h1 class="mt-1 mb-3">{{ $totalPaper }}</h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

            @if ($userInfo->user_role === 'Pengulas')
                <main class="content">
                    <div class="container-fluid p-0">

                        <h1 class="h3 mb-3"><strong>Dashboard</strong></h1>

                        <div class="row">
                            <div class="col-xl-6 col-xxl-5 d-flex">
                                <div class="w-100">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col mt-0">
                                                            <h5 class="card-title">Total Paper</h5>
                                                        </div>

                                                        <div class="col-auto">
                                                            <div class="stat text-primary">
                                                                <i class="align-middle" data-feather="dollar-sign"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <h1 class="mt-1 mb-3"><?= $totalPaper ?></h1>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col mt-0">
                                                            <h5 class="card-title">Total Paper Saya</h5>
                                                        </div>

                                                        <div class="col-auto">
                                                            <div class="stat text-primary">
                                                                <i class="align-middle" data-feather="dollar-sign"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <h1 class="mt-1 mb-3"><?= $totalPaperMe ?></h1>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                </main>
            @endif

        </div>
    </main>
@endsection
