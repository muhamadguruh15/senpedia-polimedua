@extends('layouts.layout', ['title' => 'Paper'])
@section('content')
    <main class="content">
        <div class="container-fluid p-0">

            <h1 class="h3 mb-3">Paper <strong><?= $subTitle ?></strong></h1>

            @if (session('success'))
                <div class="badge bg-success mb-3">{{ session('success') }}</div>
            @endif

            @if (session('danger'))
                <div class="badge bg-danger mb-3">{{ session('danger') }}</div>
            @endif

            <div class="row">
                <div class="col-12 d-flex">
                    <div class="card flex-fill">
                        <table class="table table-hover my-0">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Kode Paper</th>
                                    <th>Judul</th>
                                    <th>File Full Paper</th>
                                    <th>Pengarang</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no = 0;
                                @endphp
                                @foreach ($paper as $p)
                                    <tr>
                                        <td>{{ ++$no }}</td>
                                        <td>{{ $p->paper_code }}</td>
                                        <td>{{ $p->title }}</td>
                                        <td><a href="" class="btn btn-info"
                                                download="{{ $p->title }}">{{ $p->title }}</a>
                                        </td>
                                        <td>{{ $p->author_concerned }}</td>
                                        <td>
                                            <a href="{{ route('paper.show', $p->id) }}" class="btn btn-info">Detail</a>
                                            @if ($p->reviewer_id != 0)
                                                <a href="" class="btn btn-primary">Ulasan</a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
