@extends('layouts.layout', ['title' => 'Pengguna'])
@section('content')
    <main class="content">
        <div class="container-fluid p-0">

            <h1 class="h3 mb-3">Buat <strong>Pengulas</strong></h1>

            <div class="row">
                <div class="col-md-8 col-xl-9">
                    <div class="card">
                        <form action="{{ route('pengguna.store') }}" method="post">
                            @csrf
                            <div class="card-body h-100">
                                <div class="row mb-3">
                                    <div class="col-4">
                                        <label class="form-label">Nama Depan</label>
                                        <input type="text" class="form-control" name="nama_depan"
                                            value="{{ old('nama_depan') }}" placeholder="Nama depan"> <span
                                            class="text-danger small">
                                            @error('nama_depan')
                                                {{ $message }}
                                            @enderror
                                        </span>
                                    </div>
                                    <div class="col-4">
                                        <label class="form-label">Nama Tengah</label>
                                        <input type="text" class="form-control" name="nama_tengah"
                                            value="{{ old('nama_tengah') }}" placeholder="Nama tengah">
                                    </div>
                                    <div class="col-4">
                                        <label class="form-label">Nama belakang</label>
                                        <input type="text" class="form-control" name="nama_belakang"
                                            value="{{ old('nama_belakang') }}" placeholder="Nama belakang">
                                        <span class="text-danger small"> @error('nama_depan')
                                                {{ $message }}
                                            @enderror
                                        </span>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-7">
                                        <label class="form-label">Email</label>
                                        <input type="email" class="form-control" name="email"
                                            value="{{ old('email') }}" placeholder="Email">
                                        <span class="text-danger small">
                                            @error('email')
                                                {{ $message }}
                                            @enderror
                                        </span>
                                    </div>
                                    <div class="col-5">
                                        <label class="form-label">Gender</label>
                                        <select class="form-select" name="gender">
                                            <option value="">Pilih gender</option>
                                            <option <?= old('gender') == 'Laki-laki' ? 'selected' : '' ?> value="Laki-laki">
                                                Laki-laki</option>
                                            <option <?= old('gender') == 'Perempuan' ? 'selected' : '' ?> value="Perempuan">
                                                Perempuan</option>
                                        </select>
                                        <span class="text-danger small">
                                            @error('gender')
                                                {{ $message }}
                                            @enderror
                                        </span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <label class="form-label">Username</label>
                                        <input type="text" class="form-control" name="username"
                                            value="<?= old('username') ?>">
                                        <span class="text-danger small">
                                            @error('username')
                                                {{ $message }}
                                            @enderror
                                        </span>
                                    </div>
                                    <div class="col-6">
                                        <label class="form-label">Password</label>
                                        <input type="password" class="form-control" name="password">
                                        <span class="text-danger small">
                                            @error('password')
                                                {{ $message }}
                                            @enderror
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                                <button type="reset" class="btn btn-secondary">Batal</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </main>
@endsection
