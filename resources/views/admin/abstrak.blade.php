@extends('layouts.layout', ['title' => 'Abstrak'])
@section('content')
    <main class="content">
        <div class="container-fluid p-0">

            <h1 class="h3 mb-3">Daftar <strong>Abstrak</strong></h1>

            @if (session('success'))
                <div class="badge bg-success mb-3">{{ session('success') }}</div>
            @endif

            @if (session('danger'))
                <div class="badge bg-danger mb-3">{{ session('danger') }}</div>
            @endif

            <div class="row">
                <div class="col-12 d-flex">
                    <div class="card flex-fill">
                        <table class="table table-hover my-0">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>File Abstrak</th>
                                    <th>Judul</th>
                                    <th>Kategori</th>
                                    <th>Pengarang</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no = 0;
                                @endphp
                                @foreach ($abstracts as $abstract)
                                    <tr>
                                        <td>{{ ++$no }}</td>
                                        <td><a href="{{ Storage::url('/uploads/file-abstract/' . $abstract['abstract_file']) }}" class="btn btn-info">{{ $abstract->title }}</a>
                                        </td>
                                        <td>{{ $abstract->title }}</td>
                                        <td>
                                            @if ($abstract->category == 'PEKM')
                                                Penguatan Ekonomi Kreatif Masyarakat
                                            @elseif ($abstract->category == 'PTKM')
                                                Pemanfaatan Teknologi dalam Pemberdayaan Masyarakat
                                            @elseif ($abstract->category == 'PKMTKB')
                                                Peningkatan Kepedulian Masyarakat Terhadap Kesenian dan Budaya
                                            @else
                                                Pembangunan Kehidupan Masyarakat Sosial dan Berpendidikan
                                            @endif
                                        </td>
                                        <td>{{ $abstract->author_concerned }}</td>
                                        <td>
                                            <span
                                                class="badge bg-{{ $abstract['status'] == 'Diterima' ? 'success' : ($abstract['status'] == 'Ditolak' ? 'danger' : 'primary') }}">
                                                {{ $abstract['status'] }}
                                            </span>
                                        </td>

                                        <td>
                                            <a href="{{ route('abstract.show', $abstract->id) }}"
                                                class="btn btn-primary">Detail</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
