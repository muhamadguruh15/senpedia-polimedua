@extends('layouts.layout', ['title' => 'Pembayaran'])
@section('content')
    <main class="content">
        <div class="container-fluid p-0">

            <h1 class="h3 mb-3">Daftar <strong>pembayaran</strong></h1>

            @if (session('success'))
                <div class="badge bg-success mb-3">{{ session('success') }}</div>
            @endif

            @if (session('danger'))
                <div class="badge bg-danger mb-3">{{ session('danger') }}</div>
            @endif

            <div class="row">
                <div class="col-12 d-flex">
                    <div class="card flex-fill">
                        <table class="table table-hover my-0">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Akun</th>
                                    <th>Status</th>
                                    <th>Tanggal</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no = 0;
                                @endphp
                                @foreach ($pembayaran as $p)
                                    <tr>
                                        <td>{{ ++$no }}</td>
                                        <td>{{ $p->user?->full_name }}</td>
                                        <td>
                                            <span
                                                class="badge bg-{{ $p['status'] == 'Dikonfirmasi' ? 'success' : ($p['status'] == 'Ditolak' ? 'danger' : 'primary') }}">
                                                {{ $p['status'] }}
                                            </span>
                                        </td>
                                        <td>{{ $p['updated_at'] }}</td>
                                        <td>
                                            @if ($p['proof_of_payment'] !== null)
                                                <a href="{{ Storage::url('images/bukti-pembayaran/' . $p->proof_of_payment) }}"
                                                    class="btn btn-info d-inline" target="_blank">Lihat</a>
                                                <a href="{{ Storage::url('images/bukti-pembayaran/' . $p->proof_of_payment) }}"
                                                    class="btn btn-info d-inline"
                                                    download="Bukti Pembayaran - {{ $p['nama_lengkap'] }}">Download</a>

                                                @if ($p['status'] == 'Menunggu konfirmasi')
                                                    <form
                                                        action="{{ route('update.payment', ['id' => $p->id, 'status' => 'accept']) }}"
                                                        method="post" class="d-inline">
                                                        @csrf
                                                        <input type="hidden" name="id" value="{{ $p->id }}">
                                                        <input type="hidden" name="user_id" value="{{ $p->user_id }}">
                                                        <button type="submit" class="btn btn-primary"
                                                            onclick="return confirm('Konfirmasi pembayaran?')">Konfirmasi</button>
                                                    </form>
                                                    <form
                                                        action="{{ route('update.payment', ['id' => $p->id, 'status' => 'reject']) }}"
                                                        method="post" class="d-inline">
                                                        @csrf
                                                        <input type="hidden" name="id" value="{{ $p->id }}">
                                                        <input type="hidden" name="user_id" value="{{ $p->user_id }}">
                                                        <button type="submit" class="btn btn-danger"
                                                            onclick="return confirm('Tolak pembayaran?')">Tolak</button>
                                                    </form>
                                                @endif
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{-- @if ($pembayaran?->hasPages())
                            @include('components.pagination', ['paginator' => $pembayaran])
                        @endif --}}
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
