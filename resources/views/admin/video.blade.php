@extends('layouts.layout', ['title' => 'Video'])
@section('content')
    <main class="content">
        <div class="container-fluid p-0">

            <h1 class="h3 mb-3"><strong>Video</strong></h1>

            <div class="row">
                <div class="col-xl-12 d-flex">
                    <div class="w-100">
                        <div class="row">
                            @foreach ($video as $v)
                                <div class="col-6">
                                    <div class="card">
                                        <div class="card-header flex">
                                            <h5 class="card-title"><?= $v['judul'] ?></h5>
                                            @if ($v->link_video)
                                                <a href="{{ $v->link_video }}" class="btn btn-primary">Buka link</a>
                                            @endif
                                        </div>
                                        <div class="card-body">
                                            <div class="mb-3">
                                                <label>Link Video</label>
                                                <input type="text" class="form-control" value="{{ $v->link_video }}"
                                                    disabled>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </main>
@endsection
