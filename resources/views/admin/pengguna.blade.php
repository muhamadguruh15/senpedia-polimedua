@extends('layouts.layout', ['title' => 'Pengguna'])
@section('content')
    <main class="content">
        <div class="container-fluid p-0">

            <h1 class="h3 mb-3">Daftar <strong>{{ $subTitle }}</strong></h1>

            @if (session('success'))
                <div class="badge bg-success mb-3">{{ session('success') }}</div>
            @endif

            @if (session('danger'))
                <div class="badge bg-danger mb-3">{{ session('danger') }}</div>
            @endif

            <div class="row">
                <div class="col-12 d-flex">
                    <div class="card flex-fill">
                        @if ($subTitle === 'Pengulas')
                            <div class="card-header">
                                <a href="{{ route('pengguna.pengulas.form') }}" class="btn btn-info">Buat Pengulas</a>
                            </div>
                        @endif
                        <div class="card-body">
                            <table class="table table-hover my-0">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th class="d-none d-xl-table-cell">Nama Lengkap</th>
                                        <th class="d-none d-xl-table-cell">Username</th>
                                        <th>Email</th>
                                        <th class="d-none d-md-table-cell">Gender</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $no = 0;
                                    @endphp
                                    @foreach ($pengguna as $p)
                                        <tr>
                                            <td>{{ ++$no }}</td>
                                            <td class="d-none d-xl-table-cell">{{ $p->first_name . ' ' . $p->last_name }}
                                            </td>
                                            <td class="d-none d-xl-table-cell">{{ $p->username }}</td>
                                            <td><span class="badge bg-success">{{ $p->email }}</span></td>
                                            <td class="d-none d-md-table-cell">{{ $p->gender }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
