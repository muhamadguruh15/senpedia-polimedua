@extends('layouts.layout', ['title' => 'Profile'])
@section('content')
    <main class="content">
        <div class="container-fluid p-0">

            <div class="mb-3">
                <h1 class="h3 d-inline align-middle">Profil</h1>
            </div>
            @if (session('success'))
                <div class="badge bg-success mb-3">{{ session('success') }}</div>
            @endif

            @if (session('danger'))
                <div class="badge bg-danger mb-3">{{ session('danger') }}</div>
            @endif

            <div class="row">
                <div class="col-md-4 col-xl-3">
                    <div class="card mb-3">
                        <div class="card-header">
                            <h5 class="card-title mb-0">Hi, {{ $userInfo['nama_lengkap'] }}</h5>
                        </div>
                        <div class="card-body text-center">
                            <img src="{{ asset('assets/adminkit-3-2-0/src/img/avatars/avatar.jpg') }}"
                                alt="{{ $userInfo['full_name'] }}" class="img-fluid rounded-circle mb-2" width="128"
                                height="128" />
                            <h5 class="card-title mb-0">{{ $userInfo['full_name'] }}</h5>
                            <div class="text-muted mb-2">{{ $userInfo['user_role'] }}</div>
                        </div>
                    </div>
                </div>

                <div class="col-md-8 col-xl-9">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title mb-0">Ubah Profil</h5>
                        </div>
                        <form action="{{ route('profile.update') }}" method="post">
                            @csrf
                            <div class="card-body h-100">
                                <div class="row mb-3">
                                    <div class="col-4">
                                        <label class="form-label">Nama Depan</label>
                                        <input type="text" class="form-control" name="nama_depan"
                                            value="{{ old('nama_depan', $userInfo['first_name']) }}"
                                            placeholder="Nama depan">
                                        <span class="text-danger small">{{ $errors->first('nama_depan') }}</span>
                                    </div>
                                    <div class="col-4">
                                        <label class="form-label">Nama Tengah</label>
                                        <input type="text" class="form-control" name="nama_tengah"
                                            value="{{ old('nama_tengah', $userInfo['middle_name']) }}"
                                            placeholder="Nama tengah">
                                    </div>
                                    <div class="col-4">
                                        <label class="form-label">Nama belakang</label>
                                        <input type="text" class="form-control" name="nama_belakang"
                                            value="{{ old('nama_belakang', $userInfo['last_name']) }}"
                                            placeholder="Nama belakang">
                                        <span class="text-danger small">{{ $errors->first('nama_belakang') }}</span>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-7">
                                        <label class="form-label">Email</label>
                                        <input type="text" class="form-control" name="email"
                                            value="{{ old('email', $userInfo['email']) }}" placeholder="Email">
                                        <span class="text-danger small">{{ $errors->first('email') }}</span>
                                    </div>
                                    <div class="col-5">
                                        <label class="form-label">Gender</label>
                                        <select class="form-select" name="gender">
                                            <option value="">Pilih gender</option>
                                            <option value="Laki-laki"
                                                {{ old('gender', $userInfo['gender']) == 'Laki-laki' ? 'selected' : '' }}>
                                                Laki-laki
                                            </option>
                                            <option value="Perempuan"
                                                {{ old('gender', $userInfo['gender']) == 'Perempuan' ? 'selected' : '' }}>
                                                Perempuan
                                            </option>
                                        </select>
                                        <span class="text-danger small">{{ $errors->first('gender') }}</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <label class="form-label">Role</label>
                                        <input type="text" class="form-control" value="{{ $userInfo['user_role'] }}"
                                            disabled>
                                    </div>
                                    <div class="col-6">
                                        <label class="form-label">Username</label>
                                        <input type="text" class="form-control" value="{{ $userInfo['username'] }}"
                                            disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Ubah</button>
                                <button type="reset" class="btn btn-secondary">Batal</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </main>
@endsection
