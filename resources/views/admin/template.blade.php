@extends('layouts.layout', ['title' => 'Template'])
@section('content')
    <main class="content">
        <div class="container-fluid p-0">

            <h1 class="h3 mb-3"><strong>Template</strong></h1>
            @if (session('success'))
                <div class="badge bg-success mb-3">{{ session('success') }}</div>
            @endif

            @if (session('danger'))
                <div class="badge bg-danger mb-3">{{ session('danger') }}</div>
            @endif


            <div class="row">
                <div class="col-xl-12 d-flex">
                    <div class="w-100">
                        <div class="row">
                            @foreach ($template as $temp)
                                <div class="col-md-6">
                                    <div class="card">
                                        <div class="card-header">
                                            <h5 class="card-title">{{ $temp->file_name }}</h5>
                                        </div>
                                        <div class="card-body">
                                            <form action="{{ route('file.upload') }}" method="POST"
                                                enctype="multipart/form-data">
                                                @csrf
                                                <label>File</label>
                                                <input type="hidden" class="form-control" name="id_template"
                                                    value="{{ $temp->id }}">
                                                <input type="hidden" class="form-control" name="file_template_lama"
                                                    value="{{ $temp->file }}">
                                                <input type="file" class="form-control mb-3" name="file_template"
                                                    accept=".doc,.docx,application/msword">
                                                <span class="text-danger small">
                                                    @error('file_template')
                                                        {{ $message }}
                                                    @enderror
                                                </span>
                                                <br>
                                                <button class=" btn btn-primary mr-1" type="submit">Upload</button>
                                                <a href="{{ Storage::url('/uploads/file-abstract/' . $temp['file']) }}"
                                                    class=" btn btn-info mr-1">Download</a>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </main>
@endsection
