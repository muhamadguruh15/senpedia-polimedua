@extends('layouts.layout', ['title' => 'Abstrak'])
@section('content')
    <main class="content">
        <div class="container-fluid p-0">

            <h1 class="h3 mb-3">Detail <strong>Abstrak</strong></h1>

            @if (session('success'))
                <div class="badge bg-success mb-3">{{ session('success') }}</div>
            @endif

            @if (session('danger'))
                <div class="badge bg-danger mb-3">{{ session('danger') }}</div>
            @endif

            <div class="row">
                <div class="col-xl-12 col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Detail Paper</h4>
                        </div>
                        <div class="card-body">
                            <div class="section-title text-primary mt-3 mb-3">Abstrak</div>
                            <div class="mb-3">
                                <label>File Abstrak</label>
                                <br>
                                <a href="{{ Storage::url('/uploads/file-abstract/' . $abstract['abstract_file']) }}"
                                    class="btn btn-info">Abstrak - <?= $abstract['title'] ?></a>
                            </div>
                            <div class="section-title text-primary mt-3 mb-3">Paper</div>
                            <div class="mb-3">
                                <label>Kategori</label>
                                <select class="form-control" disabled>
                                    <option value="PEKM" <?= $abstract['category'] == 'PEKM' ? 'selected' : '' ?>>
                                        Penguatan Ekonomi Kreatif Masyarakat</option>
                                    <option value="PTPM" <?= $abstract['category'] == 'PTPM' ? 'selected' : '' ?>>
                                        Pemanfaatan Teknologi dalam Pemberdayaan Masyarakat</option>
                                    <option value="PKMTKB" <?= $abstract['category'] == 'PKMTKB' ? 'selected' : '' ?>>
                                        Peningkatan Kepedulian Masyarakat Terhadap Kesenian dan Budaya</option>
                                    <option value="PKMSB" <?= $abstract['category'] == 'PKMSB' ? 'selected' : '' ?>>
                                        Pembangunan Kehidupan Masyarakat Sosial dan Berpendidikan</option>
                                </select>
                            </div>
                            <div class="mb-3">
                                <label>Judul</label>
                                <input type="text" class="form-control" value="<?= $abstract['title'] ?>" disabled>
                            </div>
                            <div class="mb-3">
                                <label>Abstrak</label>
                                <textarea class="form-control" disabled><?= $abstract['abstract'] ?></textarea>
                            </div>
                            <div class="mb-3">
                                <label>Kata Kunci</label>
                                <input type="text" class="form-control" value="<?= $abstract['key_word'] ?>" disabled>
                            </div>
                            <div class="mb-3">
                                <label>Pengarang Bersangkutan</label>
                                <input type="text" class="form-control" value="<?= $abstract['author_concerned'] ?>"
                                    disabled>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h4>Metadata Paper</h4>
                        </div>
                        <div class="card-body">
                            <div class="section-title text-primary mt-3 mb-3">Pengarang 1</div>
                            <div class="row">
                                <div class="col-4">
                                    <div class="mb-3">
                                        <label>Nama Depan</label>
                                        <input type="text" class="form-control"
                                            value="{{ $abstract->paper_metadatas->first_name_author1 }}" disabled>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="mb-3">
                                        <label>Nama Tengah</label>
                                        <input type="text" class="form-control"
                                            value="{{ $abstract->paper_metadatas->middle_name_author1 }}" disabled>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="mb-3">
                                        <label>Nama Belakang</label>
                                        <input type="text" class="form-control"
                                            value="{{ $abstract->paper_metadatas->last_name_author1 }}" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-3">
                                <label>E-Mail</label>
                                <input type="text" class="form-control"
                                    value="{{ $abstract->paper_metadatas->email_author1 }}" disabled>
                            </div>
                            <div class="mb-3">
                                <label>Institusi</label>
                                <input type="text" class="form-control"
                                    value="{{ $abstract->paper_metadatas->institution_author1 }}" disabled>
                            </div>
                            <div class="mb-3">
                                <label>Pernyataan Bio</label>
                                <input type="text" class="form-control"
                                    value="{{ $abstract->paper_metadatas->author_bio_statement1 }}" disabled>
                            </div>
                            <div class="section-title text-primary mt-3 mb-3">Pengarang 2</div>
                            <div class="row">
                                <div class="col-4">
                                    <div class="mb-3">
                                        <label>Nama Depan</label>
                                        <input type="text" class="form-control"
                                            value="{{ $abstract->paper_metadatas->first_name_author2 }}" disabled>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="mb-3">
                                        <label>Nama Tengah</label>
                                        <input type="text" class="form-control"
                                            value="{{ $abstract->paper_metadatas->middle_name_author2 }}" disabled>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="mb-3">
                                        <label>Nama Belakang</label>
                                        <input type="text" class="form-control"
                                            value="{{ $abstract->paper_metadatas->last_name_author2 }}" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-3">
                                <label>E-Mail</label>
                                <input type="text" class="form-control"
                                    value="{{ $abstract->paper_metadatas->email_author2 }}" disabled>
                            </div>
                            <div class="mb-3">
                                <label>Institusi</label>
                                <input type="text" class="form-control"
                                    value="{{ $abstract->paper_metadatas->institution_author2 }}" disabled>
                            </div>
                            <div class="mb-3">
                                <label>Pernyataan Bio</label>
                                <input type="text" class="form-control"
                                    value="{{ $abstract->paper_metadatas->author_bio_statement2 }}" disabled>
                            </div>
                            <div class="section-title text-primary mt-3 mb-3">Pengarang 3</div>
                            <div class="row">
                                <div class="col-4">
                                    <div class="mb-3">
                                        <label>Nama Depan</label>
                                        <input type="text" class="form-control"
                                            value="{{ $abstract->paper_metadatas->first_name_author3 }}" disabled>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="mb-3">
                                        <label>Nama Tengah</label>
                                        <input type="text" class="form-control"
                                            value="{{ $abstract->paper_metadatas->middle_name_author3 }}" disabled>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="mb-3">
                                        <label>Nama Belakang</label>
                                        <input type="text" class="form-control"
                                            value="{{ $abstract->paper_metadatas->last_name_author3 }}" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-3">
                                <label>E-Mail</label>
                                <input type="text" class="form-control"
                                    value="{{ $abstract->paper_metadatas->email_author3 }}" disabled>
                            </div>
                            <div class="mb-3">
                                <label>Institusi</label>
                                <input type="text" class="form-control"
                                    value="{{ $abstract->paper_metadatas->institution_author3 }}" disabled>
                            </div>
                            <div class="mb-3">
                                <label>Pernyataan Bio</label>
                                <input type="text" class="form-control"
                                    value="{{ $abstract->paper_metadatas->author_bio_statement3 }}" disabled>
                            </div>
                            <div class="section-title text-primary mt-3 mb-3">Pengarang 4</div>
                            <div class="row">
                                <div class="col-4">
                                    <div class="mb-3">
                                        <label>Nama Depan</label>
                                        <input type="text" class="form-control"
                                            value="{{ $abstract->paper_metadatas->first_name_author4 }}" disabled>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="mb-3">
                                        <label>Nama Tengah</label>
                                        <input type="text" class="form-control"
                                            value="{{ $abstract->paper_metadatas->middle_name_author4 }}" disabled>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="mb-3">
                                        <label>Nama Belakang</label>
                                        <input type="text" class="form-control"
                                            value="{{ $abstract->paper_metadatas->last_name_author4 }}" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-3">
                                <label>E-Mail</label>
                                <input type="text" class="form-control"
                                    value="{{ $abstract->paper_metadatas->email_author4 }}" disabled>
                            </div>
                            <div class="mb-3">
                                <label>Institusi</label>
                                <input type="text" class="form-control"
                                    value="{{ $abstract->paper_metadatas->institution_author4 }}" disabled>
                            </div>
                            <div class="mb-3">
                                <label>Pernyataan Bio</label>
                                <input type="text" class="form-control"
                                    value="{{ $abstract->paper_metadatas->author_bio_statement4 }}" disabled>
                            </div>
                            <div class="section-title text-primary mt-3 mb-3">Pengarang 5</div>
                            <div class="row">
                                <div class="col-4">
                                    <div class="mb-3">
                                        <label>Nama Depan</label>
                                        <input type="text" class="form-control"
                                            value="{{ $abstract->paper_metadatas->first_name_author5 }}" disabled>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="mb-3">
                                        <label>Nama Tengah</label>
                                        <input type="text" class="form-control"
                                            value="{{ $abstract->paper_metadatas->middle_name_author5 }}" disabled>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="mb-3">
                                        <label>Nama Belakang</label>
                                        <input type="text" class="form-control"
                                            value="{{ $abstract->paper_metadatas->last_name_author5 }}" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-3">
                                <label>E-Mail</label>
                                <input type="text" class="form-control"
                                    value="{{ $abstract->paper_metadatas->email_author5 }}" disabled>
                            </div>
                            <div class="mb-3">
                                <label>Institusi</label>
                                <input type="text" class="form-control"
                                    value="{{ $abstract->paper_metadatas->institution_author5 }}" disabled>
                            </div>
                            <div class="mb-3">
                                <label>Pernyataan Bio</label>
                                <input type="text" class="form-control"
                                    value="{{ $abstract->paper_metadatas->author_bio_statement5 }}" disabled>
                            </div>
                            <div class="section-title text-primary mt-3 mb-3">Pengarang 6</div>
                            <div class="row">
                                <div class="col-4">
                                    <div class="mb-3">
                                        <label>Nama Depan</label>
                                        <input type="text" class="form-control"
                                            value="{{ $abstract->paper_metadatas->first_name_author6 }}" disabled>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="mb-3">
                                        <label>Nama Tengah</label>
                                        <input type="text" class="form-control"
                                            value="{{ $abstract->paper_metadatas->middle_name_author6 }}" disabled>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="mb-3">
                                        <label>Nama Belakang</label>
                                        <input type="text" class="form-control"
                                            value="{{ $abstract->paper_metadatas->last_name_author6 }}" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-3">
                                <label>E-Mail</label>
                                <input type="text" class="form-control"
                                    value="{{ $abstract->paper_metadatas->email_author6 }}" disabled>
                            </div>
                            <div class="mb-3">
                                <label>Institusi</label>
                                <input type="text" class="form-control"
                                    value="{{ $abstract->paper_metadatas->institution_author6 }}" disabled>
                            </div>
                            <div class="mb-3">
                                <label>Pernyataan Bio</label>
                                <input type="text" class="form-control"
                                    value="{{ $abstract->paper_metadatas->author_bio_statement6 }}" disabled>
                            </div>
                            <div class="section-title text-primary mt-3 mb-3">Pengarang 7</div>
                            <div class="row">
                                <div class="col-4">
                                    <div class="mb-3">
                                        <label>Nama Depan</label>
                                        <input type="text" class="form-control"
                                            value="{{ $abstract->paper_metadatas->first_name_author7 }}" disabled>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="mb-3">
                                        <label>Nama Tengah</label>
                                        <input type="text" class="form-control"
                                            value="{{ $abstract->paper_metadatas->middle_name_author7 }}" disabled>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="mb-3">
                                        <label>Nama Belakang</label>
                                        <input type="text" class="form-control"
                                            value="{{ $abstract->paper_metadatas->last_name_author7 }}" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-3">
                                <label>E-Mail</label>
                                <input type="text" class="form-control"
                                    value="{{ $abstract->paper_metadatas->email_author7 }}" disabled>
                            </div>
                            <div class="mb-3">
                                <label>Institusi</label>
                                <input type="text" class="form-control"
                                    value="{{ $abstract->paper_metadatas->institution_author7 }}" disabled>
                            </div>
                            <div class="mb-3">
                                <label>Pernyataan Bio</label>
                                <input type="text" class="form-control"
                                    value="{{ $abstract->paper_metadatas->author_bio_statement7 }}" disabled>
                            </div>
                        </div>
                        @if ($abstract->status == 'Sedang diproses')
                            <div class="card-footer text-right">
                                <form action="/admin/terima_abstrak" method="post" enctype="multipart/form-data"
                                    class="d-inline">
                                    <input type="hidden" class="form-control" name="id"
                                        value="<?= $abstract['id'] ?>">
                                    <input type="hidden" class="form-control" name="id_pengarang"
                                        value="<?= $abstract['author_id'] ?>">
                                    <div class="mb-3">
                                        <label>File LOA</label>
                                        <input type="file" class="form-control" name="file_loa"
                                            accept=".doc,.docx,application/msword,application/pdf,application/vnd.ms-excel">

                                        <span class="text-danger small">
                                            @error('file_loa')
                                                {{ $message }}
                                            @enderror
                                    </div>
                                    <button type="submit" class="btn btn-primary"
                                        onclick="return confirm('Terima abstrak?')">Terima</button>
                                </form>
                                <form action="/admin/tolak_abstrak" method="post" class="d-inline">
                                    <input type="hidden" class="form-control" name="id"
                                        value="<?= $abstract['id'] ?>">
                                    <input type="hidden" class="form-control" name="id_pengarang"
                                        value="<?= $abstract['id_pengarang'] ?>">
                                    <button type="submit" class="btn btn-danger"
                                        onclick="return confirm('Tolak abstrak?')">Tolak</button>
                                </form>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
