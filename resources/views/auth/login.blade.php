<x-guest-layout>
    <main class="d-flex w-100">
        <div class="container d-flex flex-column">
            <div class="row vh-100">
                <div class="col-sm-10 col-md-8 col-lg-6 mx-auto d-table h-100">
                    <div class="d-table-cell align-middle">
                        <div class="card">
                            <div class="card-body">
                                <div class="m-sm-4">
                                    <div class="text-center mb-3">
                                        <a href="{{ route('home') }}"> <img
                                                src="{{ asset('assets/images/logo_senpedia.png') }}" alt="Senpedia"
                                                class="img-fluid b-logo" height="132"></a>

                                    </div>

                                    <form action="{{ route('login') }}" method="POST">
                                        @csrf
                                        <div>
                                            <x-label for="username" value="{{ __('Username') }}" />
                                            <x-input id="username" class="block mt-1 w-full" type="text"
                                                name="username" :value="old('username')" required autofocus
                                                autocomplete="username" />
                                            @error('username')
                                                <div class="mb-4 font-medium text-sm text-red-600 dark:text-red-400">
                                                    {{ $message }}
                                                </div>
                                            @enderror
                                        </div>

                                        <div class="mt-4">
                                            <x-label for="password" value="{{ __('Password') }}" />
                                            <x-input id="password" class="block mt-1 w-full" type="password"
                                                name="password" required autocomplete="current-password" />
                                        </div>
                                        <div class="text-center mt-3">
                                            <x-button class="ms-4 btn btn-lg btn-primary">
                                                {{ __('Masuk') }}
                                            </x-button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="card-footer text-center">
                                Copyright &copy; 2022 <a href="https://polimedia.ac.id/">Politeknik Negeri Media
                                    Kreatif</a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </main>
</x-guest-layout>
