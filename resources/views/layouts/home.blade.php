<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description"
        content="Sistem seminar nasional pengabdian kepada masyarakat Politeknik Negeri Media Kreatif">
    <meta name="keywords" content="Seminar, Pengabdian">
    <meta name="author" content="Authentic Tech">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=5.0">
    <meta name="generator" content="Themefisher Promodise Template v1.0">
    <title>SENPEDIA | Seminar Nasional Pengabdian Masyarakat Polimedia 2022</title>

    <link rel="stylesheet" href="{{ asset('assets/promodise-main/plugins/bootstrap/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/promodise-main/plugins/fontawesome/css/all.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/promodise-main/plugins/icofont/icofont.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/promodise-main/css/style.css') }}">

    <!--Favicon-->
    <link rel="icon" href="{{ asset('assets/images/favicon_senpedia.png') }}" type="image/x-icon">

    <style>
        /* The actual timeline (the vertical ruler) */
        .timeline {
            position: relative;
            max-width: 1200px;
            margin: 0 auto;
        }

        /* The actual timeline (the vertical ruler) */
        .timeline::after {
            content: '';
            position: absolute;
            width: 6px;
            background-color: rgb(44, 20, 128);
            top: 0;
            bottom: 0;
            left: 50%;
            margin-left: -3px;
        }

        /* Container around content */
        .timeline-container {
            padding: 10px 40px;
            position: relative;
            background-color: inherit;
            width: 50%;
        }

        /* The circles on the timeline */
        .timeline-container::after {
            content: '';
            position: absolute;
            width: 25px;
            height: 25px;
            right: -17px;
            background-color: #FF9F55;
            border: 4px solid #FF9F55;
            top: 15px;
            border-radius: 50%;
            z-index: 1;
        }

        /* Place the container to the left */
        .left {
            left: 0;
        }

        /* Place the container to the right */
        .right {
            left: 50%;
        }

        /* Add arrows to the left container (pointing right) */
        .left::before {
            content: " ";
            height: 0;
            position: absolute;
            top: 22px;
            width: 0;
            z-index: 1;
            right: 30px;
            border: medium solid white;
            border-width: 10px 0 10px 10px;
            border-color: transparent transparent transparent white;
        }

        /* Fix the circle for containers on the right side */
        .left::after {
            right: -13px;
        }

        /* Add arrows to the right container (pointing left) */
        .right::before {
            content: " ";
            height: 0;
            position: absolute;
            top: 22px;
            width: 0;
            z-index: 1;
            left: 30px;
            border: medium solid white;
            border-width: 10px 10px 10px 0;
            border-color: transparent white transparent transparent;
        }

        /* Fix the circle for containers on the right side */
        .right::after {
            left: -13px;
        }

        /* The actual content */
        .timeline-content {
            padding: 20px 30px;
            background-color: white;
            position: relative;
            border-radius: 6px;
        }

        /* Media queries - Responsive timeline on screens less than 600px wide */
        @media screen and (max-width: 600px) {

            /* Place the timelime to the left */
            .timeline::after {
                left: 31px;
            }

            /* Full-width containers */
            .timeline-container {
                width: 100%;
                padding-left: 70px;
                padding-right: 25px;
            }

            /* Make sure that all arrows are pointing leftwards */
            .timeline-container::before {
                left: 60px;
                border: medium solid white;
                border-width: 10px 10px 10px 0;
                border-color: transparent white transparent transparent;
            }

            /* Make sure all circles are at the same spot */
            .left::after,
            .right::after {
                left: 15px;
            }

            /* Make all right containers behave like the left ones */
            .right {
                left: 0%;
            }
        }

        /* Sets the width for the accordion. Sets the margin to 90px on the top and bottom and auto to the left and right */

        .accordion {
            /* margin: 90px auto; */
            color: black;
            background-color: white;
            /* padding: 45px 45px; */
        }

        .accordion .accordion-container {
            position: relative;
            margin: 10px 10px;
        }

        /* Positions the labels relative to the .container. Adds padding to the top and bottom and increases font size. Also makes its cursor a pointer */

        .accordion .accordion-label {
            position: relative;
            padding: 10px 0;
            font-size: 30px;
            color: black;
            cursor: pointer;
        }

        /* Positions the plus sign 5px from the right. Centers it using the transform property. */

        .accordion .accordion-label::before {
            content: '+';
            color: black;
            position: absolute;
            top: 50%;
            right: -5px;
            font-size: 30px;
            transform: translateY(-50%);
        }

        /* Hides the content (height: 0), decreases font size, justifies text and adds transition */

        .accordion .accordion-content {
            position: relative;
            background: #FF9F55;
            height: 0;
            font-size: 20px;
            text-align: justify;
            overflow: hidden;
            transition: 0.5s;
            padding: 0px 10px;
        }

        /* Adds a horizontal line between the contents */

        .accordion hr {
            width: 100;
            margin-left: 0;
            border: 1px solid grey;
        }

        /* Unhides the content part when active. Sets the height */

        .accordion .accordion-container.active .accordion-content {
            height: 150px;
            padding: 10px;
        }

        /* Changes from plus sign to negative sign once active */

        .accordion .accordion-container.active .accordion-label::before {
            content: '-';
            font-size: 30px;
        }
    </style>

</head>

<body data-spy="scroll" data-target="#mainNav">

    <nav class="navbar navbar-expand-lg fixed-top trans-navigation">
        <div class="container">
            <a class="navbar-brand" href="{{ route('home') }}">
                <img src="{{ asset('assets/images/logo_senpedia_white.png') }}" alt="" class="img-fluid b-logo"
                    style="max-height: 75px">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mainNav"
                aria-controls="mainNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon">
                    <i class="fa fa-bars"></i>
                </span>
            </button>

            <div class="collapse navbar-collapse justify-content-end" id="mainNav">
                <ul class="navbar-nav ">
                    <li class="nav-item">
                        <a class="nav-link smoth-scroll"
                            href="{{ Request::is('/') ? '#beranda' : route('home') }}">Beranda</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link smoth-scroll"
                            href="{{ Request::is('/') ? '#panitia' : route('home', '#panitia') }}">Panitia</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link smoth-scroll"
                            href="{{ Request::is('/') ? '#faq' : route('home', '#faq') }}">FAQ</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#!" id="navbarWelcome" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Acara <i class="fas fa-chevron-down"></i>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarWelcome">
                            <li><a class="dropdown-item" href="https://senpedia2022.polimedia.ac.id/">SENPEDIA 2022</a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link smoth-scroll" href="{{ route('admisi') }}">Admisi</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link smoth-scroll" href="{{ route('payment') }}">Pembayaran</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link smoth-scroll" href="{{ route('contact') }}">Kontak</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link smoth-scroll" href="/login">Login</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!--MAIN HEADER AREA END -->

    @yield('content')


    <!--  FOOTER AREA START  -->
    <section id="footer" class="section-padding">
        <div class="container">
            <div class="row justify-content-around">
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="footer-widget footer-link">
                        <h4>Hubungi Kami</h4>

                        <p><i class="far fa-check-circle"></i> Jl. Srengseng Sawah, Kel. Srengseng Sawah, Kec.
                            Jagakarsa, Kab. Jakarta Selatan, Prov. DKI Jakarta, Indonesia, 12640</p>
                        <p><i class="far fa-check-circle"></i> +62-21-7864753/55</p>
                        <p class="mail"><i class="far fa-check-circle"></i> senpedia@polimedia.ac.id</p>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="footer-widget footer-link">
                        <h4>Didukung Oleh</h4>
                        <ul>
                            <li><a href="#!">-</a></li>
                            <li><a href="#!">-</a></li>
                            <li><a href="#!">-</a></li>
                            <li><a href="#!">-</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="footer-widget footer-text">
                        <h4>Media Sosial</h4>
                        <p><i class="far fa-check-circle"></i> <span>Instagram : senpediapolimedia</span> </p>
                        <p><i class="far fa-check-circle"></i> <span>YouTube : p3mpolimedia</span> </p>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="footer-copy">
                        Copyright &copy; 2022 <a href="https://polimedia.ac.id/">Politeknik Negeri Media Kreatif</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--  FOOTER AREA END  -->


    <!--
  Essential Scripts
  =====================================-->
    <script>
        const accordion = document.getElementsByClassName('accordion-container');

        for (i = 0; i < accordion.length; i++) {
            accordion[i].addEventListener('click', function() {
                this.classList.toggle('active')
            })
        }
    </script>


    <!-- Main jQuery -->
    <script src="{{ asset('assets/promodise-main/plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap css -->
    <script src="{{ asset('assets/promodise-main/plugins/bootstrap/bootstrap.min.js') }}"></script>

    <!-- Google Map -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcABaamniA6OL5YvYSpB3pFMNrXwXnLwU"></script>
    <script src="{{ asset('assets/promodise-main/plugins/google-map/map.js') }}"></script>

    <!-- main script -->
    <script src="{{ asset('assets/promodise-main/js/script.js') }}"></script>

</body>

</html>
