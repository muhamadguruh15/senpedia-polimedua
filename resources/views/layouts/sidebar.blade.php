<nav id="sidebar" class="sidebar js-sidebar">
    <div class="sidebar-content js-simplebar">
        <a class="sidebar-brand" href="/beranda">
            <span class="align-middle">SENPEDIA</span>
        </a>

        <ul class="sidebar-nav">
            @if (Illuminate\Support\Facades\Auth::user()->user_role == 'Admin')
                <li class="sidebar-header">
                    Dashboard
                </li>

                <li class="sidebar-item <?= $title == 'Dashboard' ? 'active' : '' ?>">
                    <a class="sidebar-link" href="/dashboard">
                        <i class="align-middle" data-feather="sliders"></i> <span class="align-middle">Dashboard</span>
                    </a>
                </li>

                <li class="sidebar-item <?= $title == 'Template' ? 'active' : '' ?>">
                    <a class="sidebar-link" href="/template">
                        <i class="align-middle" data-feather="folder"></i> <span class="align-middle">Template</span>
                    </a>
                </li>

                <li class="sidebar-header">
                    Pengguna
                </li>

                <li class="sidebar-item <?= isset($subTitle) ? ($subTitle == 'Pengulas' ? 'active' : '') : '' ?>">
                    <a class="sidebar-link" href="/pengguna/pengulas">
                        <i class="align-middle" data-feather="users"></i> <span class="align-middle">Pengulas</span>
                    </a>
                </li>

                <li class="sidebar-item <?= isset($subTitle) ? ($subTitle == 'Pengarang' ? 'active' : '') : '' ?>">
                    <a class="sidebar-link" href="/pengguna/pengarang">
                        <i class="align-middle" data-feather="users"></i> <span class="align-middle">Pengarang</span>
                    </a>
                </li>

                <li class="sidebar-item <?= isset($subTitle) ? ($subTitle == 'Peserta' ? 'active' : '') : '' ?>">
                    <a class="sidebar-link" href="/pengguna/peserta">
                        <i class="align-middle" data-feather="users"></i> <span class="align-middle">Peserta</span>
                    </a>
                </li>

                <li class="sidebar-header">
                    Abstrak & Pembayaran
                </li>

                <li class="sidebar-item <?= $title == 'Abstrak' ? 'active' : '' ?>">
                    <a class="sidebar-link" href="/abstrak">
                        <i class="align-middle" data-feather="file"></i> <span class="align-middle">Abstrak</span>
                    </a>
                </li>

                <li class="sidebar-item <?= $title == 'Pembayaran' ? 'active' : '' ?>">
                    <a class="sidebar-link" href="/pembayaran">
                        <i class="align-middle" data-feather="dollar-sign"></i> <span
                            class="align-middle">Pembayaran</span>
                    </a>
                </li>

                <li class="sidebar-header">
                    Pengabdian Masyarakat
                </li>

                <li
                    class="sidebar-item <?= isset($subTitle) ? ($subTitle == 'Penguatan Ekonomi Kreatif Masyarakat' ? 'active' : '') : '' ?>">
                    <a class="sidebar-link" href="/paper/PEKM">
                        <i class="align-middle" data-feather="file-text"></i> <span class="align-middle">Penguatan
                            Ekonomi
                            Kreatif Masyarakat</span>
                    </a>
                </li>

                <li
                    class="sidebar-item <?= isset($subTitle) ? ($subTitle == 'Pemanfaatan Teknologi dalam Pemberdayaan Masyarakat' ? 'active' : '') : '' ?>">
                    <a class="sidebar-link" href="/paper/PTPM">
                        <i class="align-middle" data-feather="file-text"></i> <span class="align-middle">Pemanfaatan
                            Teknologi dalam Pemberdayaan Masyarakat</span>
                    </a>
                </li>

                <li
                    class="sidebar-item <?= isset($subTitle) ? ($subTitle == 'Peningkatan Kepedulian Masyarakat Terhadap Kesenian dan Budaya' ? 'active' : '') : '' ?>">
                    <a class="sidebar-link" href="/paper/PKMTKB">
                        <i class="align-middle" data-feather="file-text"></i> <span class="align-middle">Peningkatan
                            Kepedulian Masyarakat Terhadap Kesenian dan Budaya</span>
                    </a>
                </li>

                <li
                    class="sidebar-item <?= isset($subTitle) ? ($subTitle == 'Pembangunan Kehidupan Masyarakat Sosial dan Berpendidikan' ? 'active' : '') : '' ?>">
                    <a class="sidebar-link" href="/paper/PKMSB">
                        <i class="align-middle" data-feather="file-text"></i> <span class="align-middle">Pembangunan
                            Kehidupan Masyarakat Sosial dan Berpendidikan</span>
                    </a>
                </li>
            @endif

            @if (Illuminate\Support\Facades\Auth::user()->user_role == 'Pengulas')
                <li class="sidebar-header">
                    Dashboard
                </li>

                <li class="sidebar-item <?= $title == 'Dashboard' ? 'active' : '' ?>">
                    <a class="sidebar-link" href="/dashboard">
                        <i class="align-middle" data-feather="sliders"></i> <span class="align-middle">Dashboard</span>
                    </a>
                </li>

                <li class="sidebar-header">
                    Pengabdian Masyarakat
                </li>

                <li class="sidebar-item <?= $title == 'Abstrak' ? 'active' : '' ?>">
                    <a class="sidebar-link" href="/abstrak">
                        <i class="align-middle" data-feather="file"></i> <span class="align-middle">Abstrak</span>
                    </a>
                </li>

                <li
                    class="sidebar-item <?= isset($subTitle) ? ($subTitle == 'Penguatan Ekonomi Kreatif Masyarakat' ? 'active' : '') : '' ?>">
                    <a class="sidebar-link" href="/paper/PEKM">
                        <i class="align-middle" data-feather="file-text"></i> <span class="align-middle">Penguatan
                            Ekonomi
                            Kreatif Masyarakat</span>
                    </a>
                </li>

                <li
                    class="sidebar-item <?= isset($subTitle) ? ($subTitle == 'Pemanfaatan Teknologi dalam Pemberdayaan Masyarakat' ? 'active' : '') : '' ?>">
                    <a class="sidebar-link" href="/paper/PTPM">
                        <i class="align-middle" data-feather="file-text"></i> <span class="align-middle">Pemanfaatan
                            Teknologi dalam Pemberdayaan Masyarakat</span>
                    </a>
                </li>

                <li
                    class="sidebar-item <?= isset($subTitle) ? ($subTitle == 'Peningkatan Kepedulian Masyarakat Terhadap Kesenian dan Budaya' ? 'active' : '') : '' ?>">
                    <a class="sidebar-link" href="/paper/PKMTKB">
                        <i class="align-middle" data-feather="file-text"></i> <span class="align-middle">Peningkatan
                            Kepedulian Masyarakat Terhadap Kesenian dan Budaya</span>
                    </a>
                </li>

                <li
                    class="sidebar-item <?= isset($subTitle) ? ($subTitle == 'Pembangunan Kehidupan Masyarakat Sosial dan Berpendidikan' ? 'active' : '') : '' ?>">
                    <a class="sidebar-link" href="/paper/PKMSP">
                        <i class="align-middle" data-feather="file-text"></i> <span class="align-middle">Pembangunan
                            Kehidupan Masyarakat Sosial dan Berpendidikan</span>
                    </a>
                </li>

                <li class="sidebar-item <?= $title == 'Video' ? 'active' : '' ?>">
                    <a class="sidebar-link" href="/video">
                        <i class="align-middle" data-feather="video"></i> <span class="align-middle">Video</span>
                    </a>
                </li>
            @endif

            @if (Illuminate\Support\Facades\Auth::user()->user_role == 'Pengarang')
                <li class="sidebar-header">
                    Dashboard
                </li>

                <li class="sidebar-item <?= $title == 'Dashboard' ? 'active' : '' ?>">
                    <a class="sidebar-link" href="/dashboard">
                        <i class="align-middle" data-feather="sliders"></i> <span
                            class="align-middle">Dashboard</span>
                    </a>
                </li>

                <li class="sidebar-item <?= $title == 'Abstrak' ? 'active' : '' ?>">
                    <a class="sidebar-link" href="/abstrak">
                        <i class="align-middle" data-feather="file"></i> <span class="align-middle">Abstrak</span>
                    </a>
                </li>

                <li class="sidebar-item <?= $title == 'Pembayaran' ? 'active' : '' ?>">
                    <a class="sidebar-link" href="/pembayaran">
                        <i class="align-middle" data-feather="dollar-sign"></i> <span
                            class="align-middle">Pembayaran</span>
                    </a>
                </li>

                <li class="sidebar-item <?= $title == 'Paper' ? 'active' : '' ?>">
                    <a class="sidebar-link" href="/paper">
                        <i class="align-middle" data-feather="file-text"></i> <span class="align-middle">Paper</span>
                    </a>
                </li>

                <li class="sidebar-item <?= $title == 'Video' ? 'active' : '' ?>">
                    <a class="sidebar-link" href="/video">
                        <i class="align-middle" data-feather="video"></i> <span class="align-middle">Video</span>
                    </a>
                </li>

                <li class="sidebar-item <?= $title == 'Acara' ? 'active' : '' ?>">
                    <a class="sidebar-link" href="/acara">
                        <i class="align-middle" data-feather="calendar"></i> <span class="align-middle">Acara</span>
                    </a>
                </li>
            @endif


            @if (Illuminate\Support\Facades\Auth::user()->user_role == 'Peserta')
                <li class="sidebar-header">
                    Dashboard
                </li>

                <li class="sidebar-item <?= $title == 'Dashboard' ? 'active' : '' ?>">
                    <a class="sidebar-link" href="/peserta">
                        <i class="align-middle" data-feather="sliders"></i> <span
                            class="align-middle">Dashboard</span>
                    </a>
                </li>

                <li class="sidebar-item <?= $title == 'Pembayaran' ? 'active' : '' ?>">
                    <a class="sidebar-link" href="/peserta/pembayaran">
                        <i class="align-middle" data-feather="dollar-sign"></i> <span
                            class="align-middle">Pembayaran</span>
                    </a>
                </li>

                <li class="sidebar-item <?= $title == 'Acara' ? 'active' : '' ?>">
                    <a class="sidebar-link" href="/peserta/acara">
                        <i class="align-middle" data-feather="calendar"></i> <span class="align-middle">Acara</span>
                    </a>
                </li>
            @endif

            <li class="sidebar-header">
                Aksi
            </li>

            <li class="sidebar-item">
                <a class="sidebar-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <i class="align-middle" data-feather="log-out"></i> <span class="align-middle">Logout</span>
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>
        </ul>
    </div>
</nav>
