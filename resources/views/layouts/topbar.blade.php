<div class="main">
    <nav class="navbar navbar-expand navbar-light navbar-bg">
        <a class="sidebar-toggle js-sidebar-toggle">
            <i class="hamburger align-self-center"></i>
        </a>

        <div class="navbar-collapse collapse">
            <ul class="navbar-nav navbar-align">
                <li class="nav-item dropdown">
                    <a class="nav-icon dropdown-toggle" href="#" id="alertsDropdown" data-bs-toggle="dropdown">
                        <div class="position-relative">
                            <i class="align-middle" data-feather="bell"></i>
                        </div>
                    </a>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-end py-0" aria-labelledby="alertsDropdown">
                        <div class="list-group">
                            @if (empty($notifications))
                                <div class="dropdown-menu-footer">
                                    <a href="#" class="text-muted">Belum ada pemberitahuan</a>
                                </div>
                            @else
                                @foreach ($notifications as $n)
                                    <a href="{{ url('/') . $n->url }}" class="list-group-item">
                                        <div class="row g-0 align-items-center">
                                            <div class="col-2">
                                                <i class="text-{{ in_array($n->title, ['Abstrak Diterima', 'Pembayaran Dikonfirmasi', 'Paper Diterima', 'Plagiasi Paper Diterima', 'Revisi Paper Diterima']) ? 'success' : (in_array($n->title, ['Abstrak Ditolak', 'Pembayaran Ditolak']) ? 'danger' : 'primary') }}"
                                                    data-feather="{{ in_array($n->title, ['Abstrak Disubmit', 'Abstrak Diterima', 'Abstrak Ditolak']) ? 'file' : (in_array($n->title, ['Bukti Pembayaran Diterima', 'Pembayaran Dikonfirmasi', 'Pembayaran Ditolak']) ? 'credit-card' : (in_array($n['judul'], ['Paper Disubmit', 'Paper Diterima', 'Paper Masuk']) ? 'file-text' : (in_array($n['judul'], ['Ulasan Plagiasi Paper', 'Paper Plagiasi', 'Plagiasi Paper Diterima', 'Paper Revisi', 'Ulasan Revisi Paper', 'Paper Revisi', 'Revisi Paper Diterima']) ? 'repeat' : 'bell'))) }}"></i>
                                            </div>
                                            <div class="col-10">
                                                <div class="text-dark">{{ $n->title }}</div>
                                                @if ($userInfo->user_role == 'Admin')
                                                    <div class="text-muted small mt-1">
                                                        {!! $n['paper_id'] != 0
                                                            ? $n->form->full_name . ' ' . $n->messages . '<span class="text-primary"> "' . $n->title . '"</span>'
                                                            : $n->form->full_name . ' ' . $n->messages !!}
                                                    </div>
                                                @else
                                                    <div class="text-muted small mt-1">
                                                        {!! $n['paper_id'] != 0
                                                            ? $n->messages . '<span class="text-primary"> "' . $n->paper->title . '"</span>'
                                                            : $n->messages !!}
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </a>
                                @endforeach
                            @endif
                        </div>
                    </div>

                </li>
                <li class="nav-item dropdown">
                    <a class="nav-icon dropdown-toggle d-inline-block d-sm-none" href="#"
                        data-bs-toggle="dropdown">
                        <i class="align-middle" data-feather="settings"></i>
                    </a>
                    <a class="nav-link dropdown-toggle d-none d-sm-inline-block" href="#"
                        data-bs-toggle="dropdown">
                        <img src="{{ asset('assets/images/foto-profil/' . Illuminate\Support\Facades\Auth::user()->profile_picture) }}"
                            class="avatar img-fluid rounded me-1" alt="" /> <span
                            class="text-dark">{{ Illuminate\Support\Facades\Auth::user()->full_name }}</span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-end">
                        <a class="dropdown-item" href="{{ route('profile') }}"><i class="align-middle me-1"
                                data-feather="user"></i>
                            Profil</a>
                        <a class="dropdown-item" href="{{ route('settings') }}"><i class="align-middle me-1"
                                data-feather="settings"></i> Pengaturan</a>
                        <div class="dropdown-divider"></div>
                        <form method="POST" action="{{ route('logout') }}" x-data>
                            @csrf
                            <button type="submit" class="dropdown-item">
                                <i class="align-middle me-1" data-feather="log-out"></i> Keluar
                            </button>
                        </form>
                    </div>

                </li>
            </ul>
        </div>
    </nav>
